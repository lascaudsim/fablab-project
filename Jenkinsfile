pipeline {
    agent any

    environment {
        DOCKERHUB_CREDENTIALS = 'dockerhub-credentials-id'
        DOCKERHUB_USERNAME = 'slk34'
        IMAGE_NAME = "${DOCKERHUB_USERNAME}/symfony-app"
    }

    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }
        stage('Install Tools') {
            steps {
                sh '''
                   apt-get update
                   apt-get install -y wget php php-cli php-xml php-mbstring docker.io
                   wget https://phar.phpunit.de/phpunit-9.phar
                   chmod +x phpunit-9.phar
                   mv phpunit-9.phar /usr/local/bin/phpunit
                '''
            }
        }
        stage('Install Dependencies') {
            steps {
                dir('app') {
                    sh 'composer install --prefer-dist --no-progress'
                }
            }
        }
        stage('Run Tests') {
            steps {
                dir('app') {
                    sh 'phpunit tests/Entity --log-junit test-results.xml'
                }
            }
        }
        stage('Build Docker Image') {
            steps {
                script {
                    sh 'docker build -t ${IMAGE_NAME}:${BUILD_NUMBER} .'
                }
            }
        }
        stage('Push Docker Image') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: DOCKERHUB_CREDENTIALS, usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]) {
                        sh 'docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD'

                        sh 'docker push ${IMAGE_NAME}:${BUILD_NUMBER}'
                    }
                }
            }
        }
    }

    post {
        always {
            junit 'app/test-results.xml'
        }
    }
}
