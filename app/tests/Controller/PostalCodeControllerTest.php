<?php
namespace App\Test\Controller;

use App\Entity\PostalCode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostalCodeControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/postal/code/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(PostalCode::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('PostalCode index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'postal_code[number]' => '12345',
        ]);

        self::assertResponseRedirects($this->path);

        $this->client->followRedirect();
        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new PostalCode();
        $fixture->setNumber('12345');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('PostalCode');

        // Use assertions to check that the properties are properly displayed.
        $crawler = $this->client->getCrawler();
        self::assertSame('12345', $crawler->filter('.number')->text());
    }

    public function testEdit(): void
    {
        $fixture = new PostalCode();
        $fixture->setNumber('12345');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'postal_code[number]' => '67890',
        ]);

        self::assertResponseRedirects('/postal/code/');

        $this->client->followRedirect();
        $updatedFixture = $this->repository->find($fixture->getId());
        self::assertSame('67890', $updatedFixture->getNumber());
    }

    public function testRemove(): void
    {
        $fixture = new PostalCode();
        $fixture->setNumber('12345');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/postal/code/');

        $this->client->followRedirect();
        self::assertSame(0, $this->repository->count([]));
    }
}
