<?php
namespace App\Test\Controller;

use App\Entity\Persons;
use App\Entity\Genders;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PersonsControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/persons/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Persons::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Person index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'person[lastname]' => 'Testing',
            'person[firstname]' => 'Testing',
            'person[phone]' => 'Testing',
            'person[email]' => 'testing@example.com',
            'person[is_visitor]' => true,
            'person[gender]' => 1, // Assuming 1 is a valid gender ID
            'person[postalcode]' => '12345',
            'person[city]' => 'Testing City',
        ]);

        self::assertResponseRedirects($this->path);

        $this->client->followRedirect();
        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new Persons();
        $fixture->setLastname('My Lastname');
        $fixture->setFirstname('My Firstname');
        $fixture->setPhone('1234567890');
        $fixture->setEmail('email@example.com');
        $fixture->setIsVisitor(true);

        $gender = new Genders();
        $gender->setType('Male');
        $this->manager->persist($gender);
        $fixture->setGender($gender);

        $fixture->setPostalcode('12345');
        $fixture->setCity('Test City');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Person');

        // Use assertions to check that the properties are properly displayed.
        $crawler = $this->client->getCrawler();
        self::assertSame('My Lastname', $crawler->filter('.lastname')->text());
        self::assertSame('My Firstname', $crawler->filter('.firstname')->text());
    }

    public function testEdit(): void
    {
        $fixture = new Persons();
        $fixture->setLastname('Old Lastname');
        $fixture->setFirstname('Old Firstname');
        $fixture->setPhone('1234567890');
        $fixture->setEmail('oldemail@example.com');
        $fixture->setIsVisitor(true);

        $gender = new Genders();
        $gender->setType('Male');
        $this->manager->persist($gender);
        $fixture->setGender($gender);

        $fixture->setPostalcode('12345');
        $fixture->setCity('Old City');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'person[lastname]' => 'New Lastname',
            'person[firstname]' => 'New Firstname',
            'person[phone]' => '0987654321',
            'person[email]' => 'newemail@example.com',
            'person[is_visitor]' => false,
            'person[gender]' => $gender->getId(),
            'person[postalcode]' => '54321',
            'person[city]' => 'New City',
        ]);

        self::assertResponseRedirects('/persons/');

        $this->client->followRedirect();
        $updatedFixture = $this->repository->find($fixture->getId());
        self::assertSame('New Lastname', $updatedFixture->getLastname());
        self::assertSame('New Firstname', $updatedFixture->getFirstname());
        self::assertSame('0987654321', $updatedFixture->getPhone());
        self::assertSame('newemail@example.com', $updatedFixture->getEmail());
        self::assertFalse($updatedFixture->isIsVisitor());
        self::assertSame('54321', $updatedFixture->getPostalcode());
        self::assertSame('New City', $updatedFixture->getCity());
    }

    public function testRemove(): void
    {
        $fixture = new Persons();
        $fixture->setLastname('To Be Deleted');
        $fixture->setFirstname('To Be Deleted');
        $fixture->setPhone('1234567890');
        $fixture->setEmail('tobedeleted@example.com');
        $fixture->setIsVisitor(true);

        $gender = new Genders();
        $gender->setType('Male');
        $this->manager->persist($gender);
        $fixture->setGender($gender);

        $fixture->setPostalcode('12345');
        $fixture->setCity('Delete City');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/persons/');

        $this->client->followRedirect();
        self::assertSame(0, $this->repository->count([]));
    }
}
