<?php
namespace App\Test\Controller;

use App\Entity\Events;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EventsControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/events/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Events::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Event index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'event[name_event]' => 'Testing',
            'event[start_date]' => '2022-01-01',
            'event[end_date]' => '2022-01-02',
            'event[start_hour]' => '10:00',
            'event[end_hour]' => '15:00',
            'event[description]' => 'Testing Description',
            'event[front_media]' => '/path/to/media',
            'event[is_published]' => true,
            'event[is_member_only]' => false,
            'event[max_participants]' => 100,
        ]);

        self::assertResponseRedirects($this->path);

        $this->client->followRedirect();
        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new Events();
        $fixture->setNameEvent('My Title');
        $fixture->setStartDate(new \DateTime('2022-01-01'));
        $fixture->setEndDate(new \DateTime('2022-01-02'));
        $fixture->setStartHour('10:00');
        $fixture->setEndHour('15:00');
        $fixture->setDescription('Description of the event');
        $fixture->setFrontMedia('/path/to/media');
        $fixture->setIsPublished(true);
        $fixture->setIsMemberOnly(false);
        $fixture->setMaxParticipants(100);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Event');

        self::assertStringContainsString('My Title', $this->client->getResponse()->getContent());
    }

    public function testEdit(): void
    {
        $fixture = new Events();
        $fixture->setNameEvent('My Title');
        $fixture->setStartDate(new \DateTime('2022-01-01'));
        $fixture->setEndDate(new \DateTime('2022-01-02'));
        $fixture->setStartHour('10:00');
        $fixture->setEndHour('15:00');
        $fixture->setDescription('Description of the event');
        $fixture->setFrontMedia('/path/to/media');
        $fixture->setIsPublished(true);
        $fixture->setIsMemberOnly(false);
        $fixture->setMaxParticipants(100);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'event[name_event]' => 'Something New',
            'event[start_date]' => '2023-01-01',
            'event[end_date]' => '2023-01-02',
            'event[start_hour]' => '11:00',
            'event[end_hour]' => '16:00',
            'event[description]' => 'Updated Description',
            'event[front_media]' => '/new/path/to/media',
            'event[is_published]' => false,
            'event[is_member_only]' => true,
            'event[max_participants]' => 200,
        ]);

        self::assertResponseRedirects('/events/');

        $this->client->followRedirect();
        $updatedFixture = $this->repository->find($fixture->getId());

        self::assertSame('Something New', $updatedFixture->getNameEvent());
        self::assertEquals(new \DateTime('2023-01-01'), $updatedFixture->getStartDate());
        self::assertEquals(new \DateTime('2023-01-02'), $updatedFixture->getEndDate());
        self::assertSame('11:00', $updatedFixture->getStartHour());
        self::assertSame('16:00', $updatedFixture->getEndHour());
        self::assertSame('Updated Description', $updatedFixture->getDescription());
        self::assertSame('/new/path/to/media', $updatedFixture->getFrontMedia());
        self::assertFalse($updatedFixture->getIsPublished());
        self::assertTrue($updatedFixture->getIsMemberOnly());
        self::assertSame(200, $updatedFixture->getMaxParticipants());
    }

    public function testRemove(): void
    {
        $fixture = new Events();
        $fixture->setNameEvent('To Be Deleted');
        $fixture->setStartDate(new \DateTime('2022-01-01'));
        $fixture->setEndDate(new \DateTime('2022-01-02'));
        $fixture->setStartHour('10:00');
        $fixture->setEndHour('15:00');
        $fixture->setDescription('Description of the event');
        $fixture->setFrontMedia('/path/to/media');
        $fixture->setIsPublished(true);
        $fixture->setIsMemberOnly(false);
        $fixture->setMaxParticipants(100);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/events/');

        $this->client->followRedirect();
        self::assertSame(0, $this->repository->count([]));
    }
}
