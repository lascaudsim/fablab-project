<?php
namespace App\Test\Controller;

use App\Entity\CatConsummables;
use App\Entity\Consummables;
use App\Entity\UnitsConsummable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConsummablesControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/consummables/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Consummables::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Consummable index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $catConsummable = new CatConsummables();
        $catConsummable->setNameCategory('TestCategory');
        $this->manager->persist($catConsummable);
        $unitConsummable = new UnitsConsummable();
        $unitConsummable->setNameUnit('TestUnit');
        $this->manager->persist($unitConsummable);
        $this->manager->flush();

        $this->client->submitForm('Save', [
            'consummable[name_consummable]' => 'Testing',
            'consummable[quantity]' => '10',
            'consummable[threshold]' => '5',
            'consummable[cat_consummables]' => $catConsummable->getId(),
            'consummable[unit_consummables]' => $unitConsummable->getId(),
        ]);

        self::assertResponseRedirects($this->path);

        $this->client->followRedirect();
        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new Consummables();
        $fixture->setNameConsummable('My Title');
        $fixture->setQuantity(10.0);
        $fixture->setThreshold(5.0);

        $catConsummable = new CatConsummables();
        $catConsummable->setNameCategory('TestCategory');
        $this->manager->persist($catConsummable);
        $unitConsummable = new UnitsConsummable();
        $unitConsummable->setNameUnit('TestUnit');
        $this->manager->persist($unitConsummable);
        $this->manager->flush();

        $fixture->setCatConsummables($catConsummable);
        $fixture->setUnitConsummables($unitConsummable);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Consummable');

        self::assertStringContainsString('My Title', $this->client->getResponse()->getContent());
    }

    public function testEdit(): void
    {
        $fixture = new Consummables();
        $fixture->setNameConsummable('Initial Value');
        $fixture->setQuantity(10.0);
        $fixture->setThreshold(5.0);

        $catConsummable = new CatConsummables();
        $catConsummable->setNameCategory('TestCategory');
        $this->manager->persist($catConsummable);
        $unitConsummable = new UnitsConsummable();
        $unitConsummable->setNameUnit('TestUnit');
        $this->manager->persist($unitConsummable);
        $this->manager->flush();

        $fixture->setCatConsummables($catConsummable);
        $fixture->setUnitConsummables($unitConsummable);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'consummable[name_consummable]' => 'Something New',
            'consummable[quantity]' => 20.0,
            'consummable[threshold]' => 10.0,
            'consummable[cat_consummables]' => $catConsummable->getId(),
            'consummable[unit_consummables]' => $unitConsummable->getId(),
        ]);

        self::assertResponseRedirects('/consummables/');

        $this->client->followRedirect();
        $updatedFixture = $this->repository->find($fixture->getId());

        self::assertSame('Something New', $updatedFixture->getNameConsummable());
        self::assertSame(20.0, $updatedFixture->getQuantity());
        self::assertSame(10.0, $updatedFixture->getThreshold());
    }

    public function testRemove(): void
    {
        $fixture = new Consummables();
        $fixture->setNameConsummable('To Be Deleted');
        $fixture->setQuantity(10.0);
        $fixture->setThreshold(5.0);

        $catConsummable = new CatConsummables();
        $catConsummable->setNameCategory('TestCategory');
        $this->manager->persist($catConsummable);
        $unitConsummable = new UnitsConsummable();
        $unitConsummable->setNameUnit('TestUnit');
        $this->manager->persist($unitConsummable);
        $this->manager->flush();

        $fixture->setCatConsummables($catConsummable);
        $fixture->setUnitConsummables($unitConsummable);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/consummables/');

        $this->client->followRedirect();
        self::assertSame(0, $this->repository->count([]));
    }
}
