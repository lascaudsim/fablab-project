<?php
namespace App\Test\Controller;

use App\Entity\UnitsConsummable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UnitsConsummableControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/units/consummable/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(UnitsConsummable::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('UnitsConsummable index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'units_consummable[name_unit]' => 'Testing',
        ]);

        self::assertResponseRedirects($this->path);

        $this->client->followRedirect();
        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new UnitsConsummable();
        $fixture->setNameUnit('My Title');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('UnitsConsummable');

        // Use assertions to check that the properties are properly displayed.
        $crawler = $this->client->getCrawler();
        self::assertSame('My Title', $crawler->filter('.name_unit')->text());
    }

    public function testEdit(): void
    {
        $fixture = new UnitsConsummable();
        $fixture->setNameUnit('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'units_consummable[name_unit]' => 'Something New',
        ]);

        self::assertResponseRedirects('/units/consummable/');

        $this->client->followRedirect();
        $updatedFixture = $this->repository->find($fixture->getId());
        self::assertSame('Something New', $updatedFixture->getNameUnit());
    }

    public function testRemove(): void
    {
        $fixture = new UnitsConsummable();
        $fixture->setNameUnit('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/units/consummable/');

        $this->client->followRedirect();
        self::assertSame(0, $this->repository->count([]));
    }
}
