<?php
namespace App\Test\Controller;

use App\Entity\Publications;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PublicationsControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/publications/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Publications::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Publication index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'publication[date_publication]' => '2023-01-01',
            'publication[title]' => 'Testing',
            'publication[description]' => 'Testing description',
            'publication[front_picture]' => '/path/to/picture',
            'publication[media]' => '/path/to/media',
            'publication[is_published]' => true,
            'publication[user]' => 'Testing User',
        ]);

        self::assertResponseRedirects($this->path);

        $this->client->followRedirect();
        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new Publications();
        $fixture->setDatePublication(new \DateTime('2023-01-01'));
        $fixture->setTitle('My Title');
        $fixture->setDescription('My Description');
        $fixture->setFrontPicture('/path/to/picture');
        $fixture->setMedia('/path/to/media');
        $fixture->setIsPublished(true);
        $fixture->setUser('My User');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Publication');

        // Use assertions to check that the properties are properly displayed.
        $crawler = $this->client->getCrawler();
        self::assertSame('My Title', $crawler->filter('.title')->text());
        self::assertSame('My Description', $crawler->filter('.description')->text());
    }

    public function testEdit(): void
    {
        $fixture = new Publications();
        $fixture->setDatePublication(new \DateTime('2023-01-01'));
        $fixture->setTitle('Initial Title');
        $fixture->setDescription('Initial Description');
        $fixture->setFrontPicture('/path/to/picture');
        $fixture->setMedia('/path/to/media');
        $fixture->setIsPublished(true);
        $fixture->setUser('Initial User');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'publication[date_publication]' => '2024-01-01',
            'publication[title]' => 'Updated Title',
            'publication[description]' => 'Updated Description',
            'publication[front_picture]' => '/new/path/to/picture',
            'publication[media]' => '/new/path/to/media',
            'publication[is_published]' => false,
            'publication[user]' => 'Updated User',
        ]);

        self::assertResponseRedirects('/publications/');

        $this->client->followRedirect();
        $updatedFixture = $this->repository->find($fixture->getId());
        self::assertSame('Updated Title', $updatedFixture->getTitle());
        self::assertSame('Updated Description', $updatedFixture->getDescription());
    }

    public function testRemove(): void
    {
        $fixture = new Publications();
        $fixture->setDatePublication(new \DateTime('2023-01-01'));
        $fixture->setTitle('Title to be deleted');
        $fixture->setDescription('Description to be deleted');
        $fixture->setFrontPicture('/path/to/picture');
        $fixture->setMedia('/path/to/media');
        $fixture->setIsPublished(true);
        $fixture->setUser('User to be deleted');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/publications/');

        $this->client->followRedirect();
        self::assertSame(0, $this->repository->count([]));
    }
}
