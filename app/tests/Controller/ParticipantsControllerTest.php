<?php
namespace App\Test\Controller;

use App\Entity\Events;
use App\Entity\Participants;
use App\Entity\Persons;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ParticipantsControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/participants/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Participants::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Participant index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $event = new Events();
        $this->manager->persist($event);

        $person = new Persons();
        $this->manager->persist($person);

        $this->manager->flush();

        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'participant[is_validated]' => true,
            'participant[event]' => $event->getId(),
            'participant[person]' => $person->getId(),
        ]);

        self::assertResponseRedirects($this->path);

        $this->client->followRedirect();
        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $event = new Events();
        $this->manager->persist($event);

        $person = new Persons();
        $this->manager->persist($person);

        $this->manager->flush();

        $fixture = new Participants();
        $fixture->setIsValidated(true);
        $fixture->setEvent($event);
        $fixture->setPerson($person);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Participant');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $event = new Events();
        $this->manager->persist($event);

        $person = new Persons();
        $this->manager->persist($person);

        $this->manager->flush();

        $fixture = new Participants();
        $fixture->setIsValidated(true);
        $fixture->setEvent($event);
        $fixture->setPerson($person);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));
        $this->client->submitForm('Update', [
            'participant[is_validated]' => false,
            'participant[event]' => $event->getId(),
            'participant[person]' => $person->getId(),
        ]);

        self::assertResponseRedirects('/participants/');

        $this->client->followRedirect();
        $updatedFixture = $this->repository->find($fixture->getId());
        self::assertFalse($updatedFixture->isIsValidated());
    }

    public function testRemove(): void
    {
        $event = new Events();
        $this->manager->persist($event);

        $person = new Persons();
        $this->manager->persist($person);

        $this->manager->flush();

        $fixture = new Participants();
        $fixture->setIsValidated(true);
        $fixture->setEvent($event);
        $fixture->setPerson($person);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/participants/');

        $this->client->followRedirect();
        self::assertSame(0, $this->repository->count([]));
    }
}
