<?php
namespace App\Test\Controller;

use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UsersControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/users/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Users::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('User index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'user[lastname]' => 'Testing',
            'user[firstname]' => 'Testing',
            'user[phone]' => '1234567890',
            'user[email]' => 'test@example.com',
            'user[is_visitor]' => true,
            'user[username]' => 'testuser',
            'user[roles]' => 'ROLE_USER',
            'user[password]' => 'password',
            'user[birthday]' => '2000-01-01',
            'user[street]' => '123 Test St',
            'user[adress_complement]' => 'Apt 1',
            'user[is_organization]' => false,
            'user[is_email_verified]' => true,
            'user[is_validated]' => true,
            'user[first_membership]' => '2022-01-01',
            'user[last_membership]' => '2023-01-01',
            'user[num_siret]' => '12345678901234',
            'user[name_organization]' => 'Test Org',
            'user[gender]' => 'M',
            'user[postalcode]' => '12345',
            'user[city]' => 'Test City',
            'user[consummable]' => 'Testing',
        ]);

        self::assertResponseRedirects($this->path);
        $this->client->followRedirect();

        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new Users();
        $fixture->setLastname('My Title');
        $fixture->setFirstname('My Title');
        $fixture->setPhone('1234567890');
        $fixture->setEmail('test@example.com');
        $fixture->setIsVisitor(true);
        $fixture->setUsername('My Title');
        $fixture->setRoles(['ROLE_USER']);
        $fixture->setPassword('My Title');
        $fixture->setBirthday(new \DateTime('2000-01-01'));
        $fixture->setStreet('My Title');
        $fixture->setAdressComplement('My Title');
        $fixture->setIsOrganization(false);
        $fixture->setIsEmailVerified(true);
        $fixture->setIsValidated(true);
        $fixture->setFirstMembership(new \DateTime('2022-01-01'));
        $fixture->setLastMembership(new \DateTime('2023-01-01'));
        $fixture->setNumSiret('12345678901234');
        $fixture->setNameOrganization('My Title');
        $fixture->setGender('M');
        $fixture->setPostalcode('12345');
        $fixture->setCity('My Title');
        $fixture->setConsummable('My Title');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('User');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $fixture = new Users();
        $fixture->setLastname('Value');
        $fixture->setFirstname('Value');
        $fixture->setPhone('Value');
        $fixture->setEmail('Value');
        $fixture->setIsVisitor(true);
        $fixture->setUsername('Value');
        $fixture->setRoles(['ROLE_USER']);
        $fixture->setPassword('Value');
        $fixture->setBirthday(new \DateTime('2000-01-01'));
        $fixture->setStreet('Value');
        $fixture->setAdressComplement('Value');
        $fixture->setIsOrganization(false);
        $fixture->setIsEmailVerified(true);
        $fixture->setIsValidated(true);
        $fixture->setFirstMembership(new \DateTime('2022-01-01'));
        $fixture->setLastMembership(new \DateTime('2023-01-01'));
        $fixture->setNumSiret('Value');
        $fixture->setNameOrganization('Value');
        $fixture->setGender('M');
        $fixture->setPostalcode('Value');
        $fixture->setCity('Value');
        $fixture->setConsummable('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'user[lastname]' => 'Something New',
            'user[firstname]' => 'Something New',
            'user[phone]' => 'Something New',
            'user[email]' => 'Something New',
            'user[is_visitor]' => false,
            'user[username]' => 'Something New',
            'user[roles]' => ['ROLE_ADMIN'],
            'user[password]' => 'Something New',
            'user[birthday]' => '2000-02-01',
            'user[street]' => 'Something New',
            'user[adress_complement]' => 'Something New',
            'user[is_organization]' => true,
            'user[is_email_verified]' => false,
            'user[is_validated]' => false,
            'user[first_membership]' => '2022-02-01',
            'user[last_membership]' => '2023-02-01',
            'user[num_siret]' => 'Something New',
            'user[name_organization]' => 'Something New',
            'user[gender]' => 'F',
            'user[postalcode]' => '67890',
            'user[city]' => 'Something New',
            'user[consummable]' => 'Something New',
        ]);

        self::assertResponseRedirects('/users/');
        $this->client->followRedirect();

        $updatedFixture = $this->repository->find($fixture->getId());
        self::assertSame('Something New', $updatedFixture->getLastname());
        self::assertSame('Something New', $updatedFixture->getFirstname());
        self::assertSame('Something New', $updatedFixture->getPhone());
        self::assertSame('Something New', $updatedFixture->getEmail());
        self::assertFalse($updatedFixture->isIsVisitor());
        self::assertSame('Something New', $updatedFixture->getUsername());
        self::assertSame(['ROLE_ADMIN'], $updatedFixture->getRoles());
        self::assertSame('Something New', $updatedFixture->getPassword());
        self::assertEquals(new \DateTime('2000-02-01'), $updatedFixture->getBirthday());
        self::assertSame('Something New', $updatedFixture->getStreet());
        self::assertSame('Something New', $updatedFixture->getAdressComplement());
        self::assertTrue($updatedFixture->isIsOrganization());
        self::assertFalse($updatedFixture->isIsEmailVerified());
        self::assertFalse($updatedFixture->isIsValidated());
        self::assertEquals(new \DateTime('2022-02-01'), $updatedFixture->getFirstMembership());
        self::assertEquals(new \DateTime('2023-02-01'), $updatedFixture->getLastMembership());
        self::assertSame('Something New', $updatedFixture->getNumSiret());
        self::assertSame('Something New', $updatedFixture->getNameOrganization());
        self::assertSame('F', $updatedFixture->getGender());
        self::assertSame('67890', $updatedFixture->getPostalcode());
        self::assertSame('Something New', $updatedFixture->getCity());
        self::assertSame('Something New', $updatedFixture->getConsummable());
    }

    public function testRemove(): void
    {
        $fixture = new Users();
        $fixture->setLastname('Value');
        $fixture->setFirstname('Value');
        $fixture->setPhone('Value');
        $fixture->setEmail('Value');
        $fixture->setIsVisitor(true);
        $fixture->setUsername('Value');
        $fixture->setRoles(['ROLE_USER']);
        $fixture->setPassword('Value');
        $fixture->setBirthday(new \DateTime('2000-01-01'));
        $fixture->setStreet('Value');
        $fixture->setAdressComplement('Value');
        $fixture->setIsOrganization(false);
        $fixture->setIsEmailVerified(true);
        $fixture->setIsValidated(true);
        $fixture->setFirstMembership(new \DateTime('2022-01-01'));
        $fixture->setLastMembership(new \DateTime('2023-01-01'));
        $fixture->setNumSiret('Value');
        $fixture->setNameOrganization('Value');
        $fixture->setGender('M');
        $fixture->setPostalcode('Value');
        $fixture->setCity('Value');
        $fixture->setConsummable('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/users/');
        $this->client->followRedirect();

        self::assertSame(0, $this->repository->count([]));
    }
}
