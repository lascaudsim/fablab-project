<?php
namespace App\Test\Controller;

use App\Entity\Machines;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MachinesControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/machines/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Machines::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Machine index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'machine[name_machine]' => 'Testing',
            'machine[number_machine]' => 'Testing',
            'machine[description]' => 'Testing',
            'machine[machine_picture]' => 'Testing',
            'machine[member_access]' => true,
            'machine[is_booked]' => false,
            // Ajoutez ici une valeur d'identifiant valide pour workspace
            'machine[workspace]' => 1,
        ]);

        self::assertResponseRedirects($this->path);

        $this->client->followRedirect();
        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new Machines();
        $fixture->setNameMachine('My Title');
        $fixture->setNumberMachine('123');
        $fixture->setDescription('Description');
        $fixture->setMachinePicture('/path/to/picture');
        $fixture->setMemberAccess(true);
        $fixture->setIsBooked(false);

        // Ici, vous devez créer ou récupérer un workspace existant
        // $workspace = ...;
        // $fixture->setWorkspace($workspace);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Machine');

        self::assertStringContainsString('My Title', $this->client->getResponse()->getContent());
    }

    public function testEdit(): void
    {
        $fixture = new Machines();
        $fixture->setNameMachine('My Title');
        $fixture->setNumberMachine('123');
        $fixture->setDescription('Description');
        $fixture->setMachinePicture('/path/to/picture');
        $fixture->setMemberAccess(true);
        $fixture->setIsBooked(false);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'machine[name_machine]' => 'Something New',
            'machine[number_machine]' => '1234',
            'machine[description]' => 'Updated Description',
            'machine[machine_picture]' => '/new/path/to/picture',
            'machine[member_access]' => false,
            'machine[is_booked]' => true,
            // Ajoutez ici une valeur d'identifiant valide pour workspace
            'machine[workspace]' => 1,
        ]);

        self::assertResponseRedirects('/machines/');

        $this->client->followRedirect();
        $updatedFixture = $this->repository->find($fixture->getId());

        self::assertSame('Something New', $updatedFixture->getNameMachine());
        self::assertSame('1234', $updatedFixture->getNumberMachine());
        self::assertSame('Updated Description', $updatedFixture->getDescription());
        self::assertSame('/new/path/to/picture', $updatedFixture->getMachinePicture());
        self::assertSame(false, $updatedFixture->getMemberAccess());
        self::assertSame(true, $updatedFixture->getIsBooked());
        // Ajoutez des vérifications pour le workspace si nécessaire
    }

    public function testRemove(): void
    {
        $fixture = new Machines();
        $fixture->setNameMachine('My Title');
        $fixture->setNumberMachine('123');
        $fixture->setDescription('Description');
        $fixture->setMachinePicture('/path/to/picture');
        $fixture->setMemberAccess(true);
        $fixture->setIsBooked(false);

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/machines/');

        $this->client->followRedirect();
        self::assertSame(0, $this->repository->count([]));
    }
}
