<?php
namespace App\Test\Controller;

use App\Entity\Reservations;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ReservationsControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/reservations/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = static::getContainer()->get('doctrine')->getManager();
        $this->repository = $this->manager->getRepository(Reservations::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Reservation index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'reservation[date_reservation]' => '2023-01-01',
            'reservation[start_hour]' => '10:00',
            'reservation[end_hour]' => '12:00',
            'reservation[is_validated]' => true,
            'reservation[state_reservation]' => 'Confirmed',
            'reservation[event]' => 'Testing Event',
            'reservation[workspace]' => 'Testing Workspace',
            'reservation[machine]' => 'Testing Machine',
            'reservation[user]' => 'Testing User',
        ]);

        self::assertResponseRedirects($this->path);

        $this->client->followRedirect();
        self::assertSame(1, $this->repository->count([]));
    }

    public function testShow(): void
    {
        $fixture = new Reservations();
        $fixture->setDateReservation(new \DateTime('2023-01-01'));
        $fixture->setStartHour('10:00');
        $fixture->setEndHour('12:00');
        $fixture->setIsValidated(true);
        $fixture->setStateReservation('Confirmed');
        $fixture->setEvent('Testing Event');
        $fixture->setWorkspace('Testing Workspace');
        $fixture->setMachine('Testing Machine');
        $fixture->setUser('Testing User');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Reservation');

        // Use assertions to check that the properties are properly displayed.
        $crawler = $this->client->getCrawler();
        self::assertSame('Testing Event', $crawler->filter('.event')->text());
        self::assertSame('Testing Workspace', $crawler->filter('.workspace')->text());
    }

    public function testEdit(): void
    {
        $fixture = new Reservations();
        $fixture->setDateReservation(new \DateTime('2023-01-01'));
        $fixture->setStartHour('10:00');
        $fixture->setEndHour('12:00');
        $fixture->setIsValidated(true);
        $fixture->setStateReservation('Confirmed');
        $fixture->setEvent('Initial Event');
        $fixture->setWorkspace('Initial Workspace');
        $fixture->setMachine('Initial Machine');
        $fixture->setUser('Initial User');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'reservation[date_reservation]' => '2024-01-01',
            'reservation[start_hour]' => '14:00',
            'reservation[end_hour]' => '16:00',
            'reservation[is_validated]' => false,
            'reservation[state_reservation]' => 'Pending',
            'reservation[event]' => 'Updated Event',
            'reservation[workspace]' => 'Updated Workspace',
            'reservation[machine]' => 'Updated Machine',
            'reservation[user]' => 'Updated User',
        ]);

        self::assertResponseRedirects('/reservations/');

        $this->client->followRedirect();
        $updatedFixture = $this->repository->find($fixture->getId());
        self::assertSame('Updated Event', $updatedFixture->getEvent());
        self::assertSame('Updated Workspace', $updatedFixture->getWorkspace());
    }

    public function testRemove(): void
    {
        $fixture = new Reservations();
        $fixture->setDateReservation(new \DateTime('2023-01-01'));
        $fixture->setStartHour('10:00');
        $fixture->setEndHour('12:00');
        $fixture->setIsValidated(true);
        $fixture->setStateReservation('Confirmed');
        $fixture->setEvent('Event to be deleted');
        $fixture->setWorkspace('Workspace to be deleted');
        $fixture->setMachine('Machine to be deleted');
        $fixture->setUser('User to be deleted');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/reservations/');

        $this->client->followRedirect();
        self::assertSame(0, $this->repository->count([]));
    }
}
