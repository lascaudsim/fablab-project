namespace App\Tests\Entity;

use App\Entity\Contact;
use PHPUnit\Framework\TestCase;

class ContactTest extends TestCase
{
    public function testContactEntityIsValid()
    {
        $contact = new Contact();
        $contact->setLastname('Doe');
        $contact->setFirstname('John');
        $contact->setEmail('john.doe@example.com');

        $this->assertNotEmpty($contact->getLastname());
        $this->assertNotEmpty($contact->getFirstname());
        $this->assertNotEmpty($contact->getEmail());
    }

    public function testInvalidLastname()
    {
        $contact = new Contact();
        $contact->setLastname('');

        $errors = $this->validate($contact);
        $this->assertCount(1, $errors);
    }

    public function testInvalidFirstname()
    {
        $contact = new Contact();
        $contact->setFirstname('');

        $errors = $this->validate($contact);
        $this->assertCount(1, $errors);
    }

    public function testInvalidEmail()
    {
        $contact = new Contact();
        $contact->setEmail('invalid-email');

        $errors = $this->validate($contact);
        $this->assertCount(1, $errors);
    }

    private function validate($entity)
    {
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        return $validator->validate($entity);
    }
}
