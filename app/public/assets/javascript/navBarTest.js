document.addEventListener("DOMContentLoaded", function() {
    var navbarToggleBtn = document.getElementById("navbarToggleBtn");
    var navbarTogglerIcon = document.querySelector(".navbar-toggler-icon");

    navbarToggleBtn.addEventListener("click", function() {
        navbarTogglerIcon.classList.toggle("rotate");
    });
});
