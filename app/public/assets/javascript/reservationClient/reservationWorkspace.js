document.addEventListener('DOMContentLoaded', function() {
    // Initialisation des modaux
    var reservationModal = new bootstrap.Modal(document.getElementById('reservationModal'));
    var loginModal = new bootstrap.Modal(document.getElementById('loginModal'));
    var validationModal = new bootstrap.Modal(document.getElementById('validationModal'));
    var successModal = new bootstrap.Modal(document.getElementById('successModal'));

    var today = new Date().toISOString().split('T')[0];
    document.getElementById('startDay').setAttribute('min', today);

    // Boutons de réservation pour utilisateurs validés
    document.querySelectorAll('.card__btn').forEach(function(button) {
        button.addEventListener('click', function() {
            document.getElementById('workspaceId').value = this.getAttribute('data-id');
            reservationModal.show();
        });
    });

    // Boutons pour inviter les utilisateurs non connectés à se connecter ou s'inscrire
    document.querySelectorAll('.card__btn-login').forEach(function(button) {
        button.addEventListener('click', function() {
            loginModal.show();
        });
    });

    // Boutons pour les utilisateurs non validés
    document.querySelectorAll('.card__btn-notvalidated').forEach(function(button) {
        button.addEventListener('click', function() {
            validationModal.show();
        });
    });

    // Gestion de la soumission du formulaire de réservation
    document.getElementById('reservationForm').addEventListener('submit', function(e) {
        e.preventDefault();
        var formData = new FormData(this);

        fetch('/workspace/reserve', {
            method: 'POST',
            body: formData
        }).then(response => response.json())
        .then(data => {
            reservationModal.hide(); // Utilisez hide() sur l'instance du modal

            document.getElementById('successMessage').innerText = data.message;
            successModal.show();

            successModal._element.addEventListener('hidden.bs.modal', function () {
                window.location.reload();
            });
        }).catch(error => {
            console.error('Error:', error);
            alert('An error occurred');
        });
    });
});
