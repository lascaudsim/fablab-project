
document.addEventListener('DOMContentLoaded', function () {
    let userIsValidated = {{ app.user ? (app.user.isValidated ? 'true' : 'false') : 'false' }};
    let membershipModal = new bootstrap.Modal(document.getElementById('membershipModal'));
    let areyousureModal = new bootstrap.Modal(document.getElementById('areyousureModal'));
    let newMemberModal = document.getElementById('newMemberModal') ? new bootstrap.Modal(document.getElementById('newMemberModal')) : null;
    let validationRequiredModal = new bootstrap.Modal(document.getElementById('validationRequiredModal')); // Nouveau modal pour la validation requise

    document.querySelectorAll('.btn-back-').forEach(function (button) {
        button.addEventListener('click', function () {
            let targetModalId = button.getAttribute('data-bs-target').substring(1);
            let targetModal;

            if (button.dataset.isMemberOnly === 'true' && !userIsValidated) {
                validationRequiredModal.show(); // Afficher le modal de validation requise au lieu de l'alerte
                return; // Ne pas continuer plus loin si cette condition est remplie
            }

            // Logique existante pour déterminer et afficher le modal ciblé
            switch (targetModalId) {
                case 'membershipModal':
                    targetModal = membershipModal;
                    break;
                case 'areyousureModal':
                    targetModal = areyousureModal;
                    break;
                case 'newMemberModal':
                    if (newMemberModal) {
                        targetModal = newMemberModal;
                    }
                    break;
            }

            if (targetModal) {
                targetModal.show();
            }
        });
    });

    // Actions pour les boutons "Oui" et "Non" dans les modales
    let yesButton = document.getElementById('yes');
    if (yesButton) {
        yesButton.addEventListener('click', function() {
            window.location.href = '{{ path('app_login') }}';
        });
    }

    let noButton = document.getElementById('no');
    if (noButton) {
        noButton.addEventListener('click', function() {
            window.location.href = '{{ path('event.book', {'id': event.id}) }}';
        });
    }

    let sureButton = document.getElementById('sure');
    if (sureButton) {
        sureButton.addEventListener('click', function() {
            window.location.href = '{{ path('user.event.book', {'id': event.id}) }}';
        });
    }

    let nonoButton = document.getElementById('nono');
    if (nonoButton) {
        nonoButton.addEventListener('click', function() {
            window.location.href = '{{ path('event.show', {'id': event.id}) }}';
        });
    }
});
