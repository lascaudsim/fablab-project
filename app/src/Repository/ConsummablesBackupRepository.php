<?php

namespace App\Repository;

use App\Entity\ConsummablesBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConsummablesBackup>
 *
 * @method ConsummablesBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConsummablesBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConsummablesBackup[]    findAll()
 * @method ConsummablesBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsummablesBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConsummablesBackup::class);
    }

    
    //    /**
    //     * @return ConsummablesBackup[] Returns an array of ConsummablesBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ConsummablesBackup
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
