<?php

namespace App\Repository;

use App\Entity\PublicationsBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PublicationsBackup>
 *
 * @method PublicationsBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method PublicationsBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method PublicationsBackup[]    findAll()
 * @method PublicationsBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PublicationsBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PublicationsBackup::class);
    }

    //    /**
    //     * @return PublicationsBackup[] Returns an array of PublicationsBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?PublicationsBackup
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
