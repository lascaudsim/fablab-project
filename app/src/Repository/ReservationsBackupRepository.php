<?php

namespace App\Repository;

use App\Entity\ReservationsBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ReservationsBackup>
 *
 * @method ReservationsBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReservationsBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReservationsBackup[]    findAll()
 * @method ReservationsBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationsBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReservationsBackup::class);
    }

    //    /**
    //     * @return ReservationsBackup[] Returns an array of ReservationsBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ReservationsBackup
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
