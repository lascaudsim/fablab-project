<?php

namespace App\Repository;

use App\Entity\CatConsummablesBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CatConsummablesBackup>
 *
 * @method CatConsummablesBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method CatConsummablesBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method CatConsummablesBackup[]    findAll()
 * @method CatConsummablesBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatConsummablesBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CatConsummablesBackup::class);
    }

    //    /**
    //     * @return CatConsummablesBackup[] Returns an array of CatConsummablesBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?CatConsummablesBackup
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
