<?php

namespace App\Repository;

use App\Entity\PersonsBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PersonsBackup>
 *
 * @method PersonsBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonsBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonsBackup[]    findAll()
 * @method PersonsBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonsBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonsBackup::class);
    }

    //    /**
    //     * @return PersonsBackup[] Returns an array of PersonsBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?PersonsBackup
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
