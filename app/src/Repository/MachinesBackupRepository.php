<?php

namespace App\Repository;

use App\Entity\MachinesBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MachinesBackup>
 *
 * @method MachinesBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method MachinesBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method MachinesBackup[]    findAll()
 * @method MachinesBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MachinesBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MachinesBackup::class);
    }

    //    /**
    //     * @return MachinesBackup[] Returns an array of MachinesBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('m.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?MachinesBackup
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
