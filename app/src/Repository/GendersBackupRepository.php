<?php

namespace App\Repository;

use App\Entity\GendersBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<GendersBackup>
 *
 * @method GendersBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method GendersBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method GendersBackup[]    findAll()
 * @method GendersBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GendersBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GendersBackup::class);
    }

    //    /**
    //     * @return GendersBackup[] Returns an array of GendersBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('g')
    //            ->andWhere('g.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('g.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?GendersBackup
    //    {
    //        return $this->createQueryBuilder('g')
    //            ->andWhere('g.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
