<?php

namespace App\Repository;

use App\Entity\WorkspacesBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<WorkspacesBackup>
 *
 * @method WorkspacesBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkspacesBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkspacesBackup[]    findAll()
 * @method WorkspacesBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkspacesBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorkspacesBackup::class);
    }

    //    /**
    //     * @return WorkspacesBackup[] Returns an array of WorkspacesBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('w')
    //            ->andWhere('w.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('w.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?WorkspacesBackup
    //    {
    //        return $this->createQueryBuilder('w')
    //            ->andWhere('w.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
