<?php

namespace App\Repository;

use App\Entity\EventsBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EventsBackup>
 *
 * @method EventsBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventsBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventsBackup[]    findAll()
 * @method EventsBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventsBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventsBackup::class);
    }

    //    /**
    //     * @return EventsBackup[] Returns an array of EventsBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('e.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?EventsBackup
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
