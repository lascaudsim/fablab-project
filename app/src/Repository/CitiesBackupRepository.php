<?php

namespace App\Repository;

use App\Entity\CitiesBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CitiesBackup>
 *
 * @method CitiesBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method CitiesBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method CitiesBackup[]    findAll()
 * @method CitiesBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CitiesBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CitiesBackup::class);
    }

    //    /**
    //     * @return CitiesBackup[] Returns an array of CitiesBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?CitiesBackup
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
