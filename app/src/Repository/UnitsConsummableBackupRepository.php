<?php

namespace App\Repository;

use App\Entity\UnitsConsummableBackup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UnitsConsummableBackup>
 *
 * @method UnitsConsummableBackup|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnitsConsummableBackup|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnitsConsummableBackup[]    findAll()
 * @method UnitsConsummableBackup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnitsConsummableBackupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnitsConsummableBackup::class);
    }

    //    /**
    //     * @return UnitsConsummableBackup[] Returns an array of UnitsConsummableBackup objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('u')
    //            ->andWhere('u.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('u.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?UnitsConsummableBackup
    //    {
    //        return $this->createQueryBuilder('u')
    //            ->andWhere('u.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
