<?php

namespace App\Entity;

use App\Repository\PostalCodeBackupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PostalCodeBackupRepository::class)]
class PostalCodeBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $number = null;

    #[ORM\OneToMany(targetEntity: PersonsBackup::class, mappedBy: 'postal_code_backup', orphanRemoval: true)]
    private Collection $personsBackups;

    public function __construct()
    {
        $this->personsBackups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): static
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return Collection<int, PersonsBackup>
     */
    public function getPersonsBackups(): Collection
    {
        return $this->personsBackups;
    }

    public function addPersonsBackup(PersonsBackup $personsBackup): static
    {
        if (!$this->personsBackups->contains($personsBackup)) {
            $this->personsBackups->add($personsBackup);
            $personsBackup->setPostalCodeBackup($this);
        }

        return $this;
    }

    public function removePersonsBackup(PersonsBackup $personsBackup): static
    {
        if ($this->personsBackups->removeElement($personsBackup)) {
            // set the owning side to null (unless already changed)
            if ($personsBackup->getPostalCodeBackup() === $this) {
                $personsBackup->setPostalCodeBackup(null);
            }
        }

        return $this;
    }
}
