<?php

namespace App\Entity;

use App\Repository\WorkspacesBackupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WorkspacesBackupRepository::class)]
class WorkspacesBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name_workspace = null;

    #[ORM\Column(length: 250, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(nullable: true)]
    private ?bool $member_access = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_booked = null;

    #[ORM\OneToMany(targetEntity: MachinesBackup::class, mappedBy: 'workspace_backup')]
    private Collection $machinesBackups;

    #[ORM\OneToMany(targetEntity: ReservationsBackup::class, mappedBy: 'workspace_backup')]
    private Collection $reservationsBackups;

    public function __construct()
    {
        $this->machinesBackups = new ArrayCollection();
        $this->reservationsBackups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameWorkspace(): ?string
    {
        return $this->name_workspace;
    }

    public function setNameWorkspace(string $name_workspace): static
    {
        $this->name_workspace = $name_workspace;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function isMemberAccess(): ?bool
    {
        return $this->member_access;
    }

    public function setMemberAccess(?bool $member_access): static
    {
        $this->member_access = $member_access;

        return $this;
    }

    public function isIsBooked(): ?bool
    {
        return $this->is_booked;
    }

    public function setIsBooked(?bool $is_booked): static
    {
        $this->is_booked = $is_booked;

        return $this;
    }

    /**
     * @return Collection<int, MachinesBackup>
     */
    public function getMachinesBackups(): Collection
    {
        return $this->machinesBackups;
    }

    public function addMachinesBackup(MachinesBackup $machinesBackup): static
    {
        if (!$this->machinesBackups->contains($machinesBackup)) {
            $this->machinesBackups->add($machinesBackup);
            $machinesBackup->setWorkspaceBackup($this);
        }

        return $this;
    }

    public function removeMachinesBackup(MachinesBackup $machinesBackup): static
    {
        if ($this->machinesBackups->removeElement($machinesBackup)) {
            // set the owning side to null (unless already changed)
            if ($machinesBackup->getWorkspaceBackup() === $this) {
                $machinesBackup->setWorkspaceBackup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ReservationsBackup>
     */
    public function getReservationsBackups(): Collection
    {
        return $this->reservationsBackups;
    }

    public function addReservationsBackup(ReservationsBackup $reservationsBackup): static
    {
        if (!$this->reservationsBackups->contains($reservationsBackup)) {
            $this->reservationsBackups->add($reservationsBackup);
            $reservationsBackup->setWorkspaceBackup($this);
        }

        return $this;
    }

    public function removeReservationsBackup(ReservationsBackup $reservationsBackup): static
    {
        if ($this->reservationsBackups->removeElement($reservationsBackup)) {
            // set the owning side to null (unless already changed)
            if ($reservationsBackup->getWorkspaceBackup() === $this) {
                $reservationsBackup->setWorkspaceBackup(null);
            }
        }

        return $this;
    }
}
