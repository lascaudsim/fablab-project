<?php

namespace App\Entity;

use App\Repository\ReservationsBackupRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationsBackupRepository::class)]
class ReservationsBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date_reservation = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $start_hour = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $end_hour = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_validated = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $state_reservation = null;

    #[ORM\ManyToOne(inversedBy: 'reservationsBackups')]
    private ?WorkspacesBackup $workspace_backup = null;

    #[ORM\ManyToOne(inversedBy: 'reservationsBackups')]
    private ?MachinesBackup $machine_backup = null;

    #[ORM\ManyToOne(inversedBy: 'reservationsBackups')]
    private ?EventsBackup $event_backup = null;

    #[ORM\ManyToOne(inversedBy: 'reservationsBackups')]
    #[ORM\JoinColumn(nullable: false)]
    private ?PersonsBackup $person_backup = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateReservation(): ?\DateTimeInterface
    {
        return $this->date_reservation;
    }

    public function setDateReservation(\DateTimeInterface $date_reservation): static
    {
        $this->date_reservation = $date_reservation;

        return $this;
    }

    public function getStartHour(): ?\DateTimeInterface
    {
        return $this->start_hour;
    }

    public function setStartHour(\DateTimeInterface $start_hour): static
    {
        $this->start_hour = $start_hour;

        return $this;
    }

    public function getEndHour(): ?\DateTimeInterface
    {
        return $this->end_hour;
    }

    public function setEndHour(\DateTimeInterface $end_hour): static
    {
        $this->end_hour = $end_hour;

        return $this;
    }

    public function isIsValidated(): ?bool
    {
        return $this->is_validated;
    }

    public function setIsValidated(?bool $is_validated): static
    {
        $this->is_validated = $is_validated;

        return $this;
    }

    public function getStateReservation(): ?string
    {
        return $this->state_reservation;
    }

    public function setStateReservation(?string $state_reservation): static
    {
        $this->state_reservation = $state_reservation;

        return $this;
    }

    public function getWorkspaceBackup(): ?WorkspacesBackup
    {
        return $this->workspace_backup;
    }

    public function setWorkspaceBackup(?WorkspacesBackup $workspace_backup): static
    {
        $this->workspace_backup = $workspace_backup;

        return $this;
    }

    public function getMachineBackup(): ?MachinesBackup
    {
        return $this->machine_backup;
    }

    public function setMachineBackup(?MachinesBackup $machine_backup): static
    {
        $this->machine_backup = $machine_backup;

        return $this;
    }

    public function getEventBackup(): ?EventsBackup
    {
        return $this->event_backup;
    }

    public function setEventBackup(?EventsBackup $event_backup): static
    {
        $this->event_backup = $event_backup;

        return $this;
    }

    public function getPersonBackup(): ?PersonsBackup
    {
        return $this->person_backup;
    }

    public function setPersonBackup(?PersonsBackup $person_backup): static
    {
        $this->person_backup = $person_backup;

        return $this;
    }
}
