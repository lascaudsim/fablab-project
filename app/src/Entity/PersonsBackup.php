<?php

namespace App\Entity;

use App\Repository\PersonsBackupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PersonsBackupRepository::class)]
class PersonsBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $lastname = null;

    #[ORM\Column(length: 50)]
    private ?string $firtname = null;

    #[ORM\Column(length: 150)]
    private ?string $email = null;

    #[ORM\Column(length: 250, nullable: true)]
    private ?string $password = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $birthday = null;

    #[ORM\Column(length: 25, nullable: true)]
    private ?string $phone = null;

    #[ORM\Column(length: 150, nullable: true)]
    private ?string $street = null;

    #[ORM\Column(length: 150, nullable: true)]
    private ?string $address_complement = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_organization = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $num_siret = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $name_organization = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_email_verified = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_validated = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $first_membership = null;

    #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $last_membership = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_visitor = null;

    #[ORM\OneToMany(targetEntity: PublicationsBackup::class, mappedBy: 'persons_backup')]
    private Collection $publicationsBackups;

    #[ORM\OneToMany(targetEntity: ReservationsBackup::class, mappedBy: 'person_backup', orphanRemoval: true)]
    private Collection $reservationsBackups;

    #[ORM\ManyToOne(inversedBy: 'personsBackups')]
    #[ORM\JoinColumn(nullable: false)]
    private ?PostalCodeBackup $postal_code_backup = null;

    #[ORM\ManyToOne(inversedBy: 'personsBackups')]
    #[ORM\JoinColumn(nullable: false)]
    private ?CitiesBackup $city_backup = null;

    #[ORM\ManyToOne(inversedBy: 'personsBackups')]
    #[ORM\JoinColumn(nullable: false)]
    private ?GendersBackup $gender_backup = null;


    #[ORM\ManyToMany(targetEntity: ConsummablesBackup::class, inversedBy: 'personsBackups')]
    private Collection $consummable_backup;

    #[ORM\Column(length: 180, nullable: true)]
    private ?string $username = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private ?array $roles = null;

    public function __construct()
    {
        $this->publicationsBackups = new ArrayCollection();
        $this->reservationsBackups = new ArrayCollection();
        $this->consummable_backup = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirtname(): ?string
    {
        return $this->firtname;
    }

    public function setFirtname(string $firtname): static
    {
        $this->firtname = $firtname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): static
    {
        $this->password = $password;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): static
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): static
    {
        $this->street = $street;

        return $this;
    }

    public function getAddressComplement(): ?string
    {
        return $this->address_complement;
    }

    public function setAddressComplement(?string $address_complement): static
    {
        $this->address_complement = $address_complement;

        return $this;
    }

    public function isIsOrganization(): ?bool
    {
        return $this->is_organization;
    }

    public function setIsOrganization(?bool $is_organization): static
    {
        $this->is_organization = $is_organization;

        return $this;
    }

    public function getNumSiret(): ?string
    {
        return $this->num_siret;
    }

    public function setNumSiret(?string $num_siret): static
    {
        $this->num_siret = $num_siret;

        return $this;
    }

    public function getNameOrganization(): ?string
    {
        return $this->name_organization;
    }

    public function setNameOrganization(?string $name_organization): static
    {
        $this->name_organization = $name_organization;

        return $this;
    }

    public function isIsEmailVerified(): ?bool
    {
        return $this->is_email_verified;
    }

    public function setIsEmailVerified(?bool $is_email_verified): static
    {
        $this->is_email_verified = $is_email_verified;

        return $this;
    }

    public function isIsValidated(): ?bool
    {
        return $this->is_validated;
    }

    public function setIsValidated(?bool $is_validated): static
    {
        $this->is_validated = $is_validated;

        return $this;
    }

    public function getFirstMembership(): ?\DateTimeInterface
    {
        return $this->first_membership;
    }

    public function setFirstMembership(?\DateTimeInterface $first_membership): static
    {
        $this->first_membership = $first_membership;

        return $this;
    }

    public function getLastMembership(): ?\DateTimeInterface
    {
        return $this->last_membership;
    }

    public function setLastMembership(?\DateTimeInterface $last_membership): static
    {
        $this->last_membership = $last_membership;

        return $this;
    }

    public function isIsVisitor(): ?bool
    {
        return $this->is_visitor;
    }

    public function setIsVisitor(?bool $is_visitor): static
    {
        $this->is_visitor = $is_visitor;

        return $this;
    }

    /**
     * @return Collection<int, PublicationsBackup>
     */
    public function getPublicationsBackups(): Collection
    {
        return $this->publicationsBackups;
    }

    public function addPublicationsBackup(PublicationsBackup $publicationsBackup): static
    {
        if (!$this->publicationsBackups->contains($publicationsBackup)) {
            $this->publicationsBackups->add($publicationsBackup);
            $publicationsBackup->setPersonsBackup($this);
        }

        return $this;
    }

    public function removePublicationsBackup(PublicationsBackup $publicationsBackup): static
    {
        if ($this->publicationsBackups->removeElement($publicationsBackup)) {
            // set the owning side to null (unless already changed)
            if ($publicationsBackup->getPersonsBackup() === $this) {
                $publicationsBackup->setPersonsBackup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ReservationsBackup>
     */
    public function getReservationsBackups(): Collection
    {
        return $this->reservationsBackups;
    }

    public function addReservationsBackup(ReservationsBackup $reservationsBackup): static
    {
        if (!$this->reservationsBackups->contains($reservationsBackup)) {
            $this->reservationsBackups->add($reservationsBackup);
            $reservationsBackup->setPersonBackup($this);
        }

        return $this;
    }

    public function removeReservationsBackup(ReservationsBackup $reservationsBackup): static
    {
        if ($this->reservationsBackups->removeElement($reservationsBackup)) {
            // set the owning side to null (unless already changed)
            if ($reservationsBackup->getPersonBackup() === $this) {
                $reservationsBackup->setPersonBackup(null);
            }
        }

        return $this;
    }

    public function getPostalCodeBackup(): ?PostalCodeBackup
    {
        return $this->postal_code_backup;
    }

    public function setPostalCodeBackup(?PostalCodeBackup $postal_code_backup): static
    {
        $this->postal_code_backup = $postal_code_backup;

        return $this;
    }

    public function getCityBackup(): ?CitiesBackup
    {
        return $this->city_backup;
    }

    public function setCityBackup(?CitiesBackup $city_backup): static
    {
        $this->city_backup = $city_backup;

        return $this;
    }

    public function getGenderBackup(): ?GendersBackup
    {
        return $this->gender_backup;
    }

    public function setGenderBackup(?GendersBackup $gender_backup): static
    {
        $this->gender_backup = $gender_backup;

        return $this;
    }


    /**
     * @return Collection<int, ConsummablesBackup>
     */
    public function getConsummableBackup(): Collection
    {
        return $this->consummable_backup;
    }

    public function addConsummableBackup(ConsummablesBackup $ConsummableBackup): static
    {
        if (!$this->consummable_backup->contains($ConsummableBackup)) {
            $this->consummable_backup->add($ConsummableBackup);
        }

        return $this;
    }

    public function removeConsummableBackup(ConsummablesBackup $ConsummableBackup): static
    {
        $this->consummable_backup->removeElement($ConsummableBackup);

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(?array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }
}
