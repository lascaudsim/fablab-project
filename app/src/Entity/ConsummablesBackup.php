<?php

namespace App\Entity;

use App\Repository\ConsummablesBackupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ConsummablesBackupRepository::class)]
class ConsummablesBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name_product = null;

    #[ORM\Column(nullable: true)]
    private ?float $quantity = null;

    #[ORM\Column(nullable: true)]
    private ?float $threshold = null;

    #[ORM\ManyToOne(inversedBy: 'consummablesBackups')]
    private ?UnitsConsummableBackup $unit_consummables_backup = null;

    #[ORM\ManyToOne(inversedBy: 'consummablesBackups')]
    private ?CatConsummablesBackup $cat_consummables_backup = null;

    #[ORM\ManyToMany(targetEntity: PersonsBackup::class, mappedBy: 'consummable_backup')]
    private Collection $personsBackups;

    public function __construct()
    {
        $this->personsBackups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameProduct(): ?string
    {
        return $this->name_product;
    }

    public function setNameProduct(string $name_product): static
    {
        $this->name_product = $name_product;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(?float $quantity): static
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getThreshold(): ?float
    {
        return $this->threshold;
    }

    public function setThreshold(?float $threshold): static
    {
        $this->threshold = $threshold;

        return $this;
    }

    public function getUnitConsummablesBackup(): ?UnitsConsummableBackup
    {
        return $this->unit_consummables_backup;
    }

    public function setUnitConsummablesBackup(?UnitsConsummableBackup $unit_consummables_backup): static
    {
        $this->unit_consummables_backup = $unit_consummables_backup;

        return $this;
    }

    public function getCatConsummablesBackup(): ?CatConsummablesBackup
    {
        return $this->cat_consummables_backup;
    }

    public function setCatConsummablesBackup(?CatConsummablesBackup $cat_consummables_backup): static
    {
        $this->cat_consummables_backup = $cat_consummables_backup;

        return $this;
    }

    /**
     * @return Collection<int, PersonsBackup>
     */
    public function getPersonsBackups(): Collection
    {
        return $this->personsBackups;
    }

    public function addPersonsBackup(PersonsBackup $personsBackup): static
    {
        if (!$this->personsBackups->contains($personsBackup)) {
            $this->personsBackups->add($personsBackup);
            $personsBackup->addConsummablesBackup($this);
        }

        return $this;
    }

    public function removePersonsBackup(PersonsBackup $personsBackup): static
    {
        if ($this->personsBackups->removeElement($personsBackup)) {
            $personsBackup->removeConsummablesBackup($this);
        }

        return $this;
    }
}
