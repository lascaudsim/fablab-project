<?php

namespace App\Entity;

use App\Repository\MachinesBackupRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MachinesBackupRepository::class)]
class MachinesBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $name_machine = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $number_machine = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 150, nullable: true)]
    private ?string $machine_picture = null;

    #[ORM\Column(nullable: true)]
    private ?bool $member_access = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_booked = null;

    #[ORM\ManyToOne(inversedBy: 'machinesBackups')]
    private ?WorkspacesBackup $workspace_backup = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameMachine(): ?string
    {
        return $this->name_machine;
    }

    public function setNameMachine(string $name_machine): static
    {
        $this->name_machine = $name_machine;

        return $this;
    }

    public function getNumberMachine(): ?string
    {
        return $this->number_machine;
    }

    public function setNumberMachine(?string $number_machine): static
    {
        $this->number_machine = $number_machine;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getMachinePicture(): ?string
    {
        return $this->machine_picture;
    }

    public function setMachinePicture(?string $machine_picture): static
    {
        $this->machine_picture = $machine_picture;

        return $this;
    }

    public function isMemberAccess(): ?bool
    {
        return $this->member_access;
    }

    public function setMemberAccess(?bool $member_access): static
    {
        $this->member_access = $member_access;

        return $this;
    }

    public function isIsBooked(): ?bool
    {
        return $this->is_booked;
    }

    public function setIsBooked(?bool $is_booked): static
    {
        $this->is_booked = $is_booked;

        return $this;
    }

    public function getWorkspaceBackup(): ?WorkspacesBackup
    {
        return $this->workspace_backup;
    }

    public function setWorkspaceBackup(?WorkspacesBackup $workspace_backup): static
    {
        $this->workspace_backup = $workspace_backup;

        return $this;
    }
}
