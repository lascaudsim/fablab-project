<?php

namespace App\Entity;

use App\Repository\PublicationsBackupRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PublicationsBackupRepository::class)]
class PublicationsBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date_publication = null;

    #[ORM\Column(length: 100)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 250)]
    private ?string $front_picture = null;

    #[ORM\Column(length: 250, nullable: true)]
    private ?string $media = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_published = null;

    #[ORM\ManyToOne(inversedBy: 'publicationsBackups')]
    private ?PersonsBackup $persons_backup = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->date_publication;
    }

    public function setDatePublication(\DateTimeInterface $date_publication): static
    {
        $this->date_publication = $date_publication;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getFrontPicture(): ?string
    {
        return $this->front_picture;
    }

    public function setFrontPicture(string $front_picture): static
    {
        $this->front_picture = $front_picture;

        return $this;
    }

    public function getMedia(): ?string
    {
        return $this->media;
    }

    public function setMedia(?string $media): static
    {
        $this->media = $media;

        return $this;
    }

    public function isIsPublished(): ?bool
    {
        return $this->is_published;
    }

    public function setIsPublished(?bool $is_published): static
    {
        $this->is_published = $is_published;

        return $this;
    }

    public function getPersonsBackup(): ?PersonsBackup
    {
        return $this->persons_backup;
    }

    public function setPersonsBackup(?PersonsBackup $persons_backup): static
    {
        $this->persons_backup = $persons_backup;

        return $this;
    }
}
