<?php

namespace App\Entity;

use App\Repository\CatConsummablesBackupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CatConsummablesBackupRepository::class)]
class CatConsummablesBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $name_category = null;

    #[ORM\OneToMany(targetEntity: ConsummablesBackup::class, mappedBy: 'cat_consummables_backup')]
    private Collection $consummablesBackups;

    public function __construct()
    {
        $this->consummablesBackups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameCategory(): ?string
    {
        return $this->name_category;
    }

    public function setNameCategory(?string $name_category): static
    {
        $this->name_category = $name_category;

        return $this;
    }

    /**
     * @return Collection<int, ConsummablesBackup>
     */
    public function getConsummablesBackups(): Collection
    {
        return $this->consummablesBackups;
    }

    public function addConsummablesBackup(ConsummablesBackup $consummablesBackup): static
    {
        if (!$this->consummablesBackups->contains($consummablesBackup)) {
            $this->consummablesBackups->add($consummablesBackup);
            $consummablesBackup->setIdCatConsummablesBackup($this);
        }

        return $this;
    }

    public function removeConsummablesBackup(ConsummablesBackup $consummablesBackup): static
    {
        if ($this->consummablesBackups->removeElement($consummablesBackup)) {
            // set the owning side to null (unless already changed)
            if ($consummablesBackup->getCatConsummablesBackup() === $this) {
                $consummablesBackup->setCatConsummablesBackup(null);
            }
        }

        return $this;
    }
}
