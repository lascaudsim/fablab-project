<?php

namespace App\Entity;

use App\Repository\ParticipantsBackupRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ParticipantsBackupRepository::class)]
class ParticipantsBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_validated = null;

    #[ORM\ManyToOne]
    private ?EventsBackup $event_backup = null;

    #[ORM\ManyToOne]
    private ?PersonsBackup $person_backup = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isIsValidated(): ?bool
    {
        return $this->is_validated;
    }

    public function setIsValidated(?bool $is_validated): static
    {
        $this->is_validated = $is_validated;

        return $this;
    }

    public function getEventBackup(): ?EventsBackup
    {
        return $this->event_backup;
    }

    public function setEventBackup(?EventsBackup $event_backup): static
    {
        $this->event_backup = $event_backup;

        return $this;
    }

    public function getPersonBackup(): ?PersonsBackup
    {
        return $this->person_backup;
    }

    public function setPersonBackup(?PersonsBackup $person_backup): static
    {
        $this->person_backup = $person_backup;

        return $this;
    }
}
