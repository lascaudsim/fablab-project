<?php

namespace App\Entity;

use App\Repository\GendersBackupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GendersBackupRepository::class)]
class GendersBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 25, nullable: true)]
    private ?string $type = null;

    #[ORM\OneToMany(targetEntity: PersonsBackup::class, mappedBy: 'gender_backup', orphanRemoval: true)]
    private Collection $personsBackups;

    public function __construct()
    {
        $this->personsBackups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): static
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, PersonsBackup>
     */
    public function getPersonsBackups(): Collection
    {
        return $this->personsBackups;
    }

    public function addPersonsBackup(PersonsBackup $personsBackup): static
    {
        if (!$this->personsBackups->contains($personsBackup)) {
            $this->personsBackups->add($personsBackup);
            $personsBackup->setGenderBackup($this);
        }

        return $this;
    }

    public function removePersonsBackup(PersonsBackup $personsBackup): static
    {
        if ($this->personsBackups->removeElement($personsBackup)) {
            // set the owning side to null (unless already changed)
            if ($personsBackup->getGenderBackup() === $this) {
                $personsBackup->setGenderBackup(null);
            }
        }

        return $this;
    }
}
