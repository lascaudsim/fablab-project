<?php

namespace App\Entity;

use App\Repository\CitiesBackupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CitiesBackupRepository::class)]
class CitiesBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $name_city = null;

    #[ORM\OneToMany(targetEntity: PersonsBackup::class, mappedBy: 'city_backup', orphanRemoval: true)]
    private Collection $personsBackups;

    public function __construct()
    {
        $this->personsBackups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameCity(): ?string
    {
        return $this->name_city;
    }

    public function setNameCity(?string $name_city): static
    {
        $this->name_city = $name_city;

        return $this;
    }

    /**
     * @return Collection<int, PersonsBackup>
     */
    public function getPersonsBackups(): Collection
    {
        return $this->personsBackups;
    }

    public function addPersonsBackup(PersonsBackup $personsBackup): static
    {
        if (!$this->personsBackups->contains($personsBackup)) {
            $this->personsBackups->add($personsBackup);
            $personsBackup->setCityBackup($this);
        }

        return $this;
    }

    public function removePersonsBackup(PersonsBackup $personsBackup): static
    {
        if ($this->personsBackups->removeElement($personsBackup)) {
            // set the owning side to null (unless already changed)
            if ($personsBackup->getCityBackup() === $this) {
                $personsBackup->setCityBackup(null);
            }
        }

        return $this;
    }
}
