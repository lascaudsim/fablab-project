<?php

namespace App\Entity;

use App\Repository\UnitsConsummableBackupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UnitsConsummableBackupRepository::class)]
class UnitsConsummableBackup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $name_unit = null;

    #[ORM\OneToMany(targetEntity: ConsummablesBackup::class, mappedBy: 'unit_consummables_backup')]
    private Collection $consummablesBackups;

    public function __construct()
    {
        $this->consummablesBackups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameUnit(): ?string
    {
        return $this->name_unit;
    }

    public function setNameUnit(?string $name_unit): static
    {
        $this->name_unit = $name_unit;

        return $this;
    }

    /**
     * @return Collection<int, ConsummablesBackup>
     */
    public function getConsummablesBackups(): Collection
    {
        return $this->consummablesBackups;
    }

    public function addConsummablesBackup(ConsummablesBackup $consummablesBackup): static
    {
        if (!$this->consummablesBackups->contains($consummablesBackup)) {
            $this->consummablesBackups->add($consummablesBackup);
            $consummablesBackup->setUnitConsummablesBackup($this);
        }

        return $this;
    }

    public function removeConsummablesBackup(ConsummablesBackup $consummablesBackup): static
    {
        if ($this->consummablesBackups->removeElement($consummablesBackup)) {
            // set the owning side to null (unless already changed)
            if ($consummablesBackup->getUnitConsummablesBackup() === $this) {
                $consummablesBackup->setUnitConsummablesBackup(null);
            }
        }

        return $this;
    }
}
