<?php

namespace App\Controller;

use App\Entity\Events;
use App\Repository\EventsRepository;
use App\Entity\Participants;
use App\Repository\ParticipantsRepository;
use App\Form\AdminEventType;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/admin/event')]
class AdminEventController extends AbstractController
{
    #[Route('/', name: 'app_admin_event')]
    public function index(EventsRepository $eventsRepository): Response
    {
        // $ParticipantsCounts = $eventsRepository->findByEventNumberOfParticipants();

        return $this->render('admin_event/index.html.twig', [
            'controller_name' => 'AdminEventController',
            'publishedEvents' => $eventsRepository->findAll(),

        ]);
    }

    // create new Event
    #[Route('/new', name: 'app_admin_event_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, FileUploader $fileUploader): Response
    {
        $event = new Events();
        $form = $this->createForm(AdminEventType::class, $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            if ($event->getEndDate() === null) {
                // Définir l'heure de début par défaut à 9h00
                $event->setStartHour(null);
            }
            // Vérification et ajustement de start_hour
            if ($event->getStartHour() === null) {
                // Définir l'heure de début par défaut à 9h00
                $event->setStartHour('9:00');
            }

            // Vérification et ajustement de end_hour
            if ($event->getEndHour() === null) {
                // Définir l'heure de fin par défaut à 18h00
                $event->setEndHour('18:00');
            }

            // Logique de traitement de l'image si un fichier est soumis
            if ($imageFile = $form->get('front_media')->getData()) {
                $imageFileName = $fileUploader->upload($imageFile);
                $event->setFrontMedia($imageFileName);
            }

            // Persistance de l'entité et redirection
            $entityManager->persist($event);
            $entityManager->flush();

            $this->addFlash('success', 'Un nouvel évènement a été créé avec succès.');

            return $this->redirectToRoute('app_admin_event', [], Response::HTTP_SEE_OTHER);
        }


        return $this->render('admin_event/newEvent.html.twig', [
            'event' => $event,
            'form' => $form,
        ]);
    }

    #[Route('/show/{id}', name: 'app_admin_event_show', methods: ['GET'])]
    public function show(Events $event, ParticipantsRepository $participantsRepository): Response
    {

        return $this->render('admin_event/showEvent.html.twig', [
            'event' => $event,
            'participants' => $participantsRepository->findParticipantsByEvent($event),
        ]);
    }

    #[Route('/validate-participant/{id}', name: 'app_admin_event_validate_participant', methods: ['POST'])]
    public function validateParticipant(Participants $participant, EntityManagerInterface $entityManager): Response
    {
        $participant->setIsValidated(!$participant->isIsValidated());
        $entityManager->flush();

        $this->addFlash('success', 'Le statut de validation a été modifié.');

        return $this->redirectToRoute('app_admin_event_show', ['id' => $participant->getEvent()->getId()]);
    }


    // mettre un formulaire pour changer le status de is validated

}
