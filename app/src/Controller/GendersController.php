<?php

namespace App\Controller;

use App\Entity\Genders;
use App\Form\GendersType;
use App\Repository\GendersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/genders')]
class GendersController extends AbstractController
{
    #[Route('/', name: 'app_genders_index', methods: ['GET'])]
    public function index(GendersRepository $gendersRepository): Response
    {
        return $this->render('crudTemplates/genders/index.html.twig', [
            'genders' => $gendersRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_genders_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $gender = new Genders();
        $form = $this->createForm(GendersType::class, $gender);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($gender);
            $entityManager->flush();

            return $this->redirectToRoute('app_genders_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('crudTemplates/genders/new.html.twig', [
            'gender' => $gender,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_genders_show', methods: ['GET'])]
    public function show(Genders $gender): Response
    {
        return $this->render('crudTemplates/genders/show.html.twig', [
            'gender' => $gender,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_genders_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Genders $gender, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(GendersType::class, $gender);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_genders_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('crudTemplates/genders/edit.html.twig', [
            'gender' => $gender,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_genders_delete', methods: ['POST'])]
    public function delete(Request $request, Genders $gender, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$gender->getId(), $request->request->get('_token'))) {
            $entityManager->remove($gender);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_genders_index', [], Response::HTTP_SEE_OTHER);
    }
}
