<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class AdTestController extends AbstractController
{
    #[Route('/ad', name: 'app_ad_test')]
    public function index(): Response
    {
        return $this->render('ad_test/index.html.twig', [
            'controller_name' => 'AdTestController',
        ]);
    }
}
