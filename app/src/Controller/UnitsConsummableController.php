<?php

namespace App\Controller;

use App\Entity\UnitsConsummable;
use App\Form\UnitsConsummableType;
use App\Repository\UnitsConsummableRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/units/consummable')]
class UnitsConsummableController extends AbstractController
{
    #[Route('/', name: 'app_units_consummable_index', methods: ['GET'])]
    public function index(UnitsConsummableRepository $unitsConsummableRepository): Response
    {
        return $this->render('admin_consummables/units_consummable/index.html.twig', [
            'units_consummables' => $unitsConsummableRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_units_consummable_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $unitsConsummable = new UnitsConsummable();
        $form = $this->createForm(UnitsConsummableType::class, $unitsConsummable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($unitsConsummable);
            $entityManager->flush();
            $this->addFlash('success','Créer une unitée avec succes');
            return $this->redirectToRoute('app_units_consummable_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_consummables/units_consummable/new.html.twig', [
            'units_consummable' => $unitsConsummable,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_units_consummable_show', methods: ['GET'])]
    public function show(UnitsConsummable $unitsConsummable): Response
    {
        return $this->render('admin_consummables/units_consummable/show.html.twig', [
            'units_consummable' => $unitsConsummable,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_units_consummable_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UnitsConsummable $unitsConsummable, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UnitsConsummableType::class, $unitsConsummable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success','Modifier une unitée avec succes');
            return $this->redirectToRoute('app_units_consummable_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_consummables/units_consummable/edit.html.twig', [
            'units_consummable' => $unitsConsummable,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_units_consummable_delete', methods: ['POST'])]
    public function delete(Request $request, UnitsConsummable $unitsConsummable, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$unitsConsummable->getId(), $request->request->get('_token'))) {
            $entityManager->remove($unitsConsummable);
            $entityManager->flush();
            $this->addFlash('success','Effacer une unitée avec succes');
        }

        return $this->redirectToRoute('app_units_consummable_index', [], Response::HTTP_SEE_OTHER);
    }
}
