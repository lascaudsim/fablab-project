<?php

namespace App\Controller;

use App\Entity\CatConsummables;
use App\Form\CatConsummablesType;
use App\Repository\CatConsummablesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/cat/consummables')]
class CatConsummablesController extends AbstractController
{
    #[Route('/', name: 'app_cat_consummables_index', methods: ['GET'])]
    public function index(CatConsummablesRepository $catConsummablesRepository): Response
    {
        return $this->render('admin_consummables/cat_consummables/index.html.twig', [
            'cat_consummables' => $catConsummablesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_cat_consummables_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $catConsummable = new CatConsummables();
        $form = $this->createForm(CatConsummablesType::class, $catConsummable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($catConsummable);
            $entityManager->flush();
            $this->addFlash('success','Créer un consummable avec succes');
            return $this->redirectToRoute('app_cat_consummables_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_consummables/cat_consummables/new.html.twig', [
            'cat_consummable' => $catConsummable,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_cat_consummables_show', methods: ['GET'])]
    public function show(CatConsummables $catConsummable): Response
    {
        return $this->render('admin_consummables/cat_consummables/show.html.twig', [
            'cat_consummable' => $catConsummable,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_cat_consummables_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CatConsummables $catConsummable, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CatConsummablesType::class, $catConsummable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success','Modifier un consummables avec succes');
            return $this->redirectToRoute('app_cat_consummables_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_consummables/cat_consummables/edit.html.twig', [
            'cat_consummable' => $catConsummable,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_cat_consummables_delete', methods: ['POST'])]
    public function delete(Request $request, CatConsummables $catConsummable, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$catConsummable->getId(), $request->request->get('_token'))) {
            $entityManager->remove($catConsummable);
            $entityManager->flush();
            $this->addFlash('success','Effacer un consummables avec succes');
        }

        return $this->redirectToRoute('app_cat_consummables_index', [], Response::HTTP_SEE_OTHER);
    }
}
