<?php

namespace App\Controller;

use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Entity\Cities;
use App\Repository\CitiesRepository;

class TestController extends AbstractController
{
    #[Route('/test', name: 'app_test', methods: ['GET'])]
    public function index(CitiesRepository $citiesRepository): Response
    {
        $cities = new Cities();

        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
            'cities' => $citiesRepository->findAll(),
            
        ]);
    }
}
