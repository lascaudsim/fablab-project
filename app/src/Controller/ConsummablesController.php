<?php

namespace App\Controller;

use App\Entity\Consummables;
use App\Form\ConsummablesType;
use App\Repository\ConsummablesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/consummables')]
class ConsummablesController extends AbstractController
{
    #[Route('/', name: 'app_consummables_index', methods: ['GET'])]
    public function index(ConsummablesRepository $consummablesRepository): Response
    {
        return $this->render('crudTemplates/consummables/index.html.twig', [
            'consummables' => $consummablesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_consummables_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $consummable = new Consummables();
        $form = $this->createForm(ConsummablesType::class, $consummable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($consummable);
            $entityManager->flush();

            return $this->redirectToRoute('app_consummables_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('crudTemplates/consummables/new.html.twig', [
            'consummable' => $consummable,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_consummables_show', methods: ['GET'])]
    public function show(Consummables $consummable): Response
    {
        return $this->render('crudTemplates/consummables/show.html.twig', [
            'consummable' => $consummable,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_consummables_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Consummables $consummable, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ConsummablesType::class, $consummable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_consummables_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('crudTemplates/consummables/edit.html.twig', [
            'consummable' => $consummable,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_consummables_delete', methods: ['POST'])]
    public function delete(Request $request, Consummables $consummable, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$consummable->getId(), $request->request->get('_token'))) {
            $entityManager->remove($consummable);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_consummables_index', [], Response::HTTP_SEE_OTHER);
    }
}
