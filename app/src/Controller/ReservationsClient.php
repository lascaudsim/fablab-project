<?php

namespace App\Controller;

use App\Entity\Reservations;
use App\Repository\UsersRepository;
use App\Repository\MachinesRepository;
use App\Repository\WorkspacesRepository;
use App\Repository\ReservationsRepository;
use App\Service\SendMailService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ReservationsClient extends AbstractController
{
    #[Route('/reservation/machines', name: 'machines_reservation', methods: ['GET', 'POST'])]
    public function index(MachinesRepository $machinesRepository, ReservationsRepository $reservationsRepository, WorkspacesRepository $workspacesRepository, UsersRepository $usersRepository): Response
    {
        $machines = $machinesRepository->findAll();
        $reservations = $reservationsRepository->findAll();
        $user = $this->getUser();

        return $this->render('visitor/reservation_machine/machine_reservation.html.twig', [
            'machines' => $machines,
            'user' => $user,
            'reservations' => $reservations
        ]);
    }

    #[Route('/machine/reserve', name: 'machine_reserve', methods: ['POST'])]
    public function reserveMachine(
        Request $request,
        SendMailService $mail,
        MachinesRepository $machinesRepository,
        EntityManagerInterface $em,
        UsersRepository $userRepository
    ): Response {

        $user = $this->getUser(); 
        if (!$user) {
            return $this->json(['message' => 'Utilisateur non authentifié'], Response::HTTP_UNAUTHORIZED);
        }

        $machineId = $request->request->get('machineId');
        $startHour = $request->request->get('startHour');
        $endHour = $request->request->get('endHour');
        $startDay = $request->request->get('startDay');

        $machine = $machinesRepository->find($machineId);
        if (!$machine) {
            return $this->json(['message' => 'Machine non trouvée'], Response::HTTP_NOT_FOUND);
        }

        $reservation = new Reservations();
        $reservation->setMachine($machine);
        $reservation->setUser($user);
        $reservation->setDateReservation(new \DateTime($startDay));
        $reservation->setStartHour(new \DateTime($startHour));
        $reservation->setEndHour(new \DateTime($endHour));
        $reservation->setIsValidated(false);

        $adminUser = $userRepository->findOneBy(['username' => 'admin']);
        if ($adminUser) {
            $adminEmail = $adminUser->getEmail();

            // Informations de l'utilisateur à envoyer au responsable
            $userInfo = [
                'userName' => $user->getUsername(),
                'userEmail' => $user->getEmail(),
                'machineName' => $machine->getNameMachine(),
                'reservationDate' => $reservation->getDateReservation()->format('Y-m-d'),
                'startTime' => $reservation->getStartHour()->format('H:i'),
                'endTime' => $reservation->getEndHour()->format('H:i'),
            ];
            
            $mail->send(
                'no-reply@fablab.net',
                $adminEmail,
                "Nouvelle réservation du client",
                'client_reservation',
                ['userInfo' => $userInfo]
            );
        }

        // Persister l'objet dans la base de données
        $em->persist($reservation);
        $em->flush(); // Appliquer les changements dans la base de données

        // Retourner une réponse JSON pour confirmer la création de la réservation
        return $this->json(['message' => 'Réservation enregistrée avec succès'], Response::HTTP_OK);
    }







    
    // ESPACE WORKSPACE 

    #[Route('/reservation/espace', name: 'espace_reservation', methods: ['GET', 'POST'])]
    public function workspace(WorkspacesRepository $workspacesRepository, ReservationsRepository $reservationsRepository, UsersRepository $usersRepository): Response
    {
        $workspaces = $workspacesRepository->findAll();
        $reservations = $reservationsRepository->findAll();
        $user = $this->getUser();

        return $this->render('visitor/reservation_workspace/workspace_reservation.html.twig', [
            'workspaces' => $workspaces,
            'user' => $user,
            'reservations' => $reservations
        ]);
    }

    #[Route('/workspace/reserve', name: 'workspace_reserve', methods: ['POST'])]
    public function reserveWorkspace(
        Request $request,
        SendMailService $mail,
        WorkspacesRepository $workspacesRepository,
        EntityManagerInterface $em,
        UsersRepository $userRepository
    ): Response {

        $user = $this->getUser(); 
        if (!$user) {
            return $this->json(['message' => 'Utilisateur non authentifié'], Response::HTTP_UNAUTHORIZED);
        }

        $workspaceId = $request->request->get('workspaceId');
        $startHour = $request->request->get('startHour');
        $endHour = $request->request->get('endHour');
        $startDay = $request->request->get('startDay');

        $workspace = $workspacesRepository->find($workspaceId);
        if (!$workspace) {
            return $this->json(['message' => 'Espace non trouvée'], Response::HTTP_NOT_FOUND);
        }

        $reservation = new Reservations();
        $reservation->setWorkspace($workspace);
        $reservation->setUser($user);
        $reservation->setDateReservation(new \DateTime($startDay));
        $reservation->setStartHour(new \DateTime($startHour));
        $reservation->setEndHour(new \DateTime($endHour));
        $reservation->setIsValidated(false);

        $adminUser = $userRepository->findOneBy(['username' => 'admin']);
        if ($adminUser) {
            $adminEmail = $adminUser->getEmail();

            // Informations de l'utilisateur à envoyer au responsable
            $userInfo = [
                'userName' => $user->getUsername(),
                'userEmail' => $user->getEmail(),
                'workspaceName' => $workspace->getNameWorkspace(),
                'reservationDate' => $reservation->getDateReservation()->format('Y-m-d'),
                'startTime' => $reservation->getStartHour()->format('H:i'),
                'endTime' => $reservation->getEndHour()->format('H:i'),
            ];
            
            $mail->send(
                'no-reply@fablab.net',
                $adminEmail,
                "Nouvelle réservation du client",
                'client_reservation_workspace',
                ['userInfo' => $userInfo]
            );
        }

        // Persister l'objet dans la base de données
        $em->persist($reservation);
        $em->flush(); // Appliquer les changements dans la base de données

        // Retourner une réponse JSON pour confirmer la création de la réservation
        return $this->json(['message' => 'Réservation enregistrée avec succès'], Response::HTTP_OK);
    }
}
