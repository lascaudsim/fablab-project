<?php

namespace App\Controller;

use App\Entity\Workspaces;
use App\Form\WorkspacesType;
use App\Repository\WorkspacesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/workspaces')]
class WorkspacesController extends AbstractController
{
    #[Route('/', name: 'app_workspaces_index', methods: ['GET'])]
    public function index(WorkspacesRepository $workspacesRepository): Response
    {
        return $this->render('crudTemplates/workspaces/index.html.twig', [
            'workspaces' => $workspacesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_workspaces_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $workspace = new Workspaces();
        $form = $this->createForm(WorkspacesType::class, $workspace);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($workspace);
            $entityManager->flush();

            return $this->redirectToRoute('crudTemplates/app_workspaces_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('workspaces/new.html.twig', [
            'workspace' => $workspace,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_workspaces_show', methods: ['GET'])]
    public function show(Workspaces $workspace): Response
    {
        return $this->render('crudTemplates/workspaces/show.html.twig', [
            'workspace' => $workspace,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_workspaces_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Workspaces $workspace, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(WorkspacesType::class, $workspace);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_workspaces_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('crudTemplates/workspaces/edit.html.twig', [
            'workspace' => $workspace,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_workspaces_delete', methods: ['POST'])]
    public function delete(Request $request, Workspaces $workspace, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$workspace->getId(), $request->request->get('_token'))) {
            $entityManager->remove($workspace);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_workspaces_index', [], Response::HTTP_SEE_OTHER);
    }
}
