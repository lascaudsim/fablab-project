<?php

namespace App\Controller;

use App\Entity\Users;
use App\Entity\Persons;
use App\Entity\Participants;
use App\Repository\GendersRepository;
use App\Repository\EventsRepository;
use App\Form\VisitorEventRegistrationType;
use App\Repository\ParticipantsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class VisitorEventsController extends AbstractController
{
#[Route('/visitor/events', name: 'app_visitor_events', methods: ['GET'])]
public function index(Request $request, EventsRepository $eventsRepository, ParticipantsRepository $participantsRepository): Response
{
    $events = $eventsRepository->findBy([], ['start_date' => 'ASC', 'start_hour' => 'ASC']);
    $eventData = [];

    // Vérifier s'il y a des événements
    if (!empty($events)) {
        // Compter les participants validés pour chaque événement
        foreach ($events as $event) {
            $eventData[$event->getId()] = [
                'event' => $event,
                'currentParticipants' => $participantsRepository->countValidatedParticipantsForEvent($event->getId()),
            ];
        }
    }

    return $this->render('visitor/visitor_events/index.html.twig', [
        'eventData' => $eventData,
    ]);
}



    #[Route('/visitor/events/{id}', name: 'event.show', methods: ['GET'])]
    public function show(int $id, EventsRepository $repository, ParticipantsRepository $participantsRepository): Response
    {
        $event = $repository->find($id);

        if (!$event) {
            throw $this->createNotFoundException('Cet événement n\'existe pas.');
        }

        // Comptez les participants actuels et déterminez si l'événement a atteint sa capacité maximale
        $currentParticipantsCount = $participantsRepository->countValidatedParticipantsForEvent($id);
        $isEventFull = $event->getMaxParticipants() !== null && $currentParticipantsCount >= $event->getMaxParticipants();

        return $this->render('visitor/visitor_events/show.html.twig', [
            'event' => $event,
            'isEventFull' => $isEventFull, // Transmettez cette information à votre template
        ]);
    }




    #[Route('/visitor/events/{id}/book', name: 'event.book', methods: ['GET', 'POST'])]
    public function book(Request $request, ParticipantsRepository $participantsRepository, GendersRepository $gendersRepository, int $id, EventsRepository $repository, EntityManagerInterface $em, UserInterface $user = null): Response
    {
        $event = $repository->find($id);
        $genders = $gendersRepository->findAll();


        if (!$event) {
            throw $this->createNotFoundException('Cet événement n\'existe pas.');
        }
        $currentParticipantsCount = $participantsRepository->countValidatedParticipantsForEvent($id);
        if ($event->getMaxParticipants() !== null && $currentParticipantsCount >= $event->getMaxParticipants()) {
            $this->addFlash('warning', 'Le nombre maximum de participants a été atteint pour cet événement.');
            return $this->redirectToRoute('app_visitor_events');
        }

        if ($user instanceof Users && $event->isIsMemberOnly() && !$user->isIsValidated()) {
            $this->addFlash('warning', 'Impossible de vous enregistrer vous n\'avez pas accès ici vous n\'êtes pas membre');
            return $this->redirectToRoute('app_visitor_events');
        }

        $person = new Persons();
        $form = $this->createForm(VisitorEventRegistrationType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($person);

            $participant = new Participants();
            $participant->setPerson($person);
            $participant->setEvent($event);
            $em->persist($participant);

            $em->flush();

            $this->addFlash('success', 'Nous avons bien pris en compte votre inscription.Un mail de confirmation vous sera prochainement envoyé.A bientôt');
            return $this->redirectToRoute('flash_messages');
        }

        return $this->render('visitor/visitor_events/book.html.twig', [
            'event' => $event,
            'form' => $form, // Assurez-vous d'appeler createView()
        ]);
    }


    #[Route('/flash-messages', name: 'flash_messages')]
    public function showFlashSpecial(): Response
    {
        return $this->render('visitor/visitor_events/flash_messages.html.twig');
    }
}
