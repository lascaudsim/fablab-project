<?php

namespace App\Form;

use App\Entity\Events;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminEventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('name_event', TextType::class, [
            'label' => 'Nom de l\'évènement',
            'attr' => [
                'class' => 'text-center py-4 deco-input',
                'placeholder' => 'Mettre un titre'
            ],
            'constraints' => [
                new NotBlank([
                    'message' => 'Veuillez entrer le nom de l\'évènement.',
                ]),
                new Length([
                    'max' => 50,
                    'maxMessage' => 'Le nom de l\'évènement ne peut pas dépasser {{ limit }} caractères.',
                ]),
            ],
        ])
            ->add('start_date', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date de début',
                'attr' => ['class' => 'text-center deco-input  py-4 '], // Ajoute cette classe pour centrer le texte
            ])
            
            ->add('end_date', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date de fin',
                'required' => false, 
                'attr' => ['class' => 'text-center deco-input  py-4 '], // Ajoute cette classe pour centrer le texte
            ])
            ->add('start_hour', TimeType::class, [
                'widget' => 'single_text',
                'input'  => 'string',
                'label' => "Horaire de début",
                'required' => false,
                'attr' => ['class' => 'text-center deco-input  py-4 '], // Ajoute cette classe pour centrer le texte
            ])
            ->add('end_hour', TimeType::class, [
                'widget' => 'single_text',
                'input'  => 'string',
                'label' => 'Horaire de fin',
                'required' => false,
                'attr' => ['class' => 'text-center deco-input  py-4 '], // Ajoute cette classe pour centrer le texte
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'attr' => ['class' => 'form-control deco-input'],
                'required' => true, 
                'constraints' => [
                    new Length([
                        'max' => 1000,
                        'maxMessage' => 'La description ne peut pas dépasser {{ limit }} caractères.',
                    ]),
                ],
            ])
            ->add('front_media', FileType::class, [
                'label' => 'Image de l\'évènement',
                'mapped' => false,
                'required' => false,
                'attr' => ['class' => 'text-center deco-input '], // Ajoute cette classe pour centrer le texte
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                            'image/gif',
                        ],
                        'mimeTypesMessage' => 'Veuillez télécharger un fichier image valide (JPEG, PNG, GIF).',
                    ])
                ],
            ])
            ->add('is_published', null, [
                'label' => 'Publier l\'évènement',
                'attr' => ['class' => 'form-control text-center btn-cochet']
            ])
            ->add('is_member_only', null,[
                'label' => 'Évènement réservé aux membres',
                'attr' => ['class' => 'form-control text-center btn-cochet']
            ])
            ->add('max_participants', TextType::class, [
                'attr' => ['class' => 'form-control text-center deco-input',
                'placeholder' => 'Nombre de personne(ex:10)'
            ],
                'label' => 'Nombre maximum de participants'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Events::class,
        ]);
    }
}
