<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240327220309 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cat_consummables (id INT AUTO_INCREMENT NOT NULL, name_category VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cat_consummables_backup (id INT AUTO_INCREMENT NOT NULL, name_category VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cities (id INT AUTO_INCREMENT NOT NULL, name_city VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cities_backup (id INT AUTO_INCREMENT NOT NULL, name_city VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE consummables (id INT AUTO_INCREMENT NOT NULL, cat_consummables_id INT DEFAULT NULL, unit_consummables_id INT DEFAULT NULL, name_consummable VARCHAR(100) NOT NULL, quantity DOUBLE PRECISION DEFAULT NULL, threshold DOUBLE PRECISION DEFAULT NULL, INDEX IDX_FEB893BAADC9B221 (cat_consummables_id), INDEX IDX_FEB893BABC2655D7 (unit_consummables_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE consummables_backup (id INT AUTO_INCREMENT NOT NULL, unit_consummables_backup_id INT DEFAULT NULL, cat_consummables_backup_id INT DEFAULT NULL, name_product VARCHAR(100) NOT NULL, quantity DOUBLE PRECISION DEFAULT NULL, threshold DOUBLE PRECISION DEFAULT NULL, INDEX IDX_9F3A606D67BBBA2E (unit_consummables_backup_id), INDEX IDX_9F3A606D2975FE03 (cat_consummables_backup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, lastname VARCHAR(50) DEFAULT NULL, firstname VARCHAR(50) DEFAULT NULL, email VARCHAR(150) NOT NULL, phone VARCHAR(25) DEFAULT NULL, message LONGTEXT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events (id INT AUTO_INCREMENT NOT NULL, name_event VARCHAR(50) NOT NULL, start_date DATE NOT NULL, end_date DATE DEFAULT NULL, start_hour TIME DEFAULT NULL, end_hour TIME DEFAULT NULL, description LONGTEXT DEFAULT NULL, front_media VARCHAR(150) DEFAULT NULL, is_published TINYINT(1) DEFAULT NULL, is_member_only TINYINT(1) NOT NULL, max_participants INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events_backup (id INT AUTO_INCREMENT NOT NULL, name_event VARCHAR(50) NOT NULL, start_date DATE NOT NULL, end_date DATE DEFAULT NULL, start_hour TIME DEFAULT NULL, end_hour TIME DEFAULT NULL, description LONGTEXT DEFAULT NULL, front_media VARCHAR(150) DEFAULT NULL, is_published TINYINT(1) DEFAULT NULL, is_member_only TINYINT(1) DEFAULT NULL, max_participants INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genders (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genders_backup (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(25) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE machines (id INT AUTO_INCREMENT NOT NULL, workspace_id INT DEFAULT NULL, name_machine VARCHAR(50) NOT NULL, number_machine VARCHAR(50) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, machine_picture VARCHAR(100) DEFAULT NULL, member_access TINYINT(1) DEFAULT NULL, is_booked TINYINT(1) DEFAULT NULL, INDEX IDX_F1CE8DED82D40A1F (workspace_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE machines_backup (id INT AUTO_INCREMENT NOT NULL, workspace_backup_id INT DEFAULT NULL, name_machine VARCHAR(50) NOT NULL, number_machine VARCHAR(50) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, machine_picture VARCHAR(150) DEFAULT NULL, member_access TINYINT(1) DEFAULT NULL, is_booked TINYINT(1) DEFAULT NULL, INDEX IDX_6E65D18647915D4F (workspace_backup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participants (id INT AUTO_INCREMENT NOT NULL, event_id INT DEFAULT NULL, person_id INT DEFAULT NULL, is_validated TINYINT(1) DEFAULT NULL, INDEX IDX_7169709271F7E88B (event_id), INDEX IDX_71697092217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participants_backup (id INT AUTO_INCREMENT NOT NULL, event_backup_id INT DEFAULT NULL, person_backup_id INT DEFAULT NULL, is_validated TINYINT(1) DEFAULT NULL, INDEX IDX_F2178E3E15285974 (event_backup_id), INDEX IDX_F2178E3E2B9484E3 (person_backup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE persons (id INT AUTO_INCREMENT NOT NULL, gender_id INT NOT NULL, postalcode_id INT NOT NULL, city_id INT NOT NULL, lastname VARCHAR(50) NOT NULL, firstname VARCHAR(50) NOT NULL, phone VARCHAR(50) NOT NULL, email VARCHAR(150) NOT NULL, is_visitor TINYINT(1) DEFAULT NULL, dtype VARCHAR(255) NOT NULL, username VARCHAR(180) DEFAULT NULL, roles JSON DEFAULT NULL, password VARCHAR(255) DEFAULT NULL, birthday DATE DEFAULT NULL, street VARCHAR(150) DEFAULT NULL, adress_complement VARCHAR(150) DEFAULT NULL, is_organization TINYINT(1) DEFAULT NULL, is_email_verified TINYINT(1) DEFAULT NULL, reset_token VARCHAR(100) DEFAULT NULL, is_validated TINYINT(1) DEFAULT NULL, first_membership DATE DEFAULT NULL, last_membership DATE DEFAULT NULL, num_siret VARCHAR(50) DEFAULT NULL, name_organization VARCHAR(50) DEFAULT NULL, INDEX IDX_A25CC7D3708A0E0 (gender_id), INDEX IDX_A25CC7D31CED915 (postalcode_id), INDEX IDX_A25CC7D38BAC62AF (city_id), UNIQUE INDEX UNIQ_A25CC7D3F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_consummables (users_id INT NOT NULL, consummables_id INT NOT NULL, INDEX IDX_ADFCB5B667B3B43D (users_id), INDEX IDX_ADFCB5B6315E5C6F (consummables_id), PRIMARY KEY(users_id, consummables_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE persons_backup (id INT AUTO_INCREMENT NOT NULL, postal_code_backup_id INT NOT NULL, city_backup_id INT NOT NULL, gender_backup_id INT NOT NULL, lastname VARCHAR(50) NOT NULL, firtname VARCHAR(50) NOT NULL, email VARCHAR(150) NOT NULL, password VARCHAR(250) DEFAULT NULL, birthday DATE DEFAULT NULL, phone VARCHAR(25) DEFAULT NULL, street VARCHAR(150) DEFAULT NULL, address_complement VARCHAR(150) DEFAULT NULL, is_organization TINYINT(1) DEFAULT NULL, num_siret VARCHAR(50) DEFAULT NULL, name_organization VARCHAR(50) DEFAULT NULL, is_email_verified TINYINT(1) DEFAULT NULL, is_validated TINYINT(1) DEFAULT NULL, first_membership DATE DEFAULT NULL, last_membership DATE DEFAULT NULL, is_visitor TINYINT(1) DEFAULT NULL, username VARCHAR(180) DEFAULT NULL, roles LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_4A84F2BF7D78D8B8 (postal_code_backup_id), INDEX IDX_4A84F2BF37E97B5C (city_backup_id), INDEX IDX_4A84F2BFFBA2F1A6 (gender_backup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE persons_backup_consummables_backup (persons_backup_id INT NOT NULL, consummables_backup_id INT NOT NULL, INDEX IDX_52D07603641CB851 (persons_backup_id), INDEX IDX_52D0760385BF2FC9 (consummables_backup_id), PRIMARY KEY(persons_backup_id, consummables_backup_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE postal_code (id INT AUTO_INCREMENT NOT NULL, number INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE postal_code_backup (id INT AUTO_INCREMENT NOT NULL, number INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE publications (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, date_publication DATE NOT NULL, title VARCHAR(100) NOT NULL, description LONGTEXT DEFAULT NULL, front_picture VARCHAR(255) DEFAULT NULL, media VARCHAR(255) DEFAULT NULL, is_published TINYINT(1) DEFAULT NULL, INDEX IDX_32783AF4A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE publications_backup (id INT AUTO_INCREMENT NOT NULL, persons_backup_id INT DEFAULT NULL, date_publication DATE NOT NULL, title VARCHAR(100) NOT NULL, description LONGTEXT DEFAULT NULL, front_picture VARCHAR(250) NOT NULL, media VARCHAR(250) DEFAULT NULL, is_published TINYINT(1) DEFAULT NULL, INDEX IDX_6093970A641CB851 (persons_backup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservations (id INT AUTO_INCREMENT NOT NULL, event_id INT DEFAULT NULL, workspace_id INT NOT NULL, machine_id INT DEFAULT NULL, user_id INT NOT NULL, date_reservation DATE NOT NULL, start_hour TIME NOT NULL, end_hour TIME NOT NULL, is_validated TINYINT(1) DEFAULT NULL, state_reservation VARCHAR(50) DEFAULT NULL, INDEX IDX_4DA23971F7E88B (event_id), INDEX IDX_4DA23982D40A1F (workspace_id), INDEX IDX_4DA239F6B75B26 (machine_id), INDEX IDX_4DA239A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservations_backup (id INT AUTO_INCREMENT NOT NULL, workspace_backup_id INT DEFAULT NULL, machine_backup_id INT DEFAULT NULL, event_backup_id INT DEFAULT NULL, person_backup_id INT NOT NULL, date_reservation DATE NOT NULL, start_hour TIME NOT NULL, end_hour TIME NOT NULL, is_validated TINYINT(1) DEFAULT NULL, state_reservation VARCHAR(50) DEFAULT NULL, INDEX IDX_F8FFAB347915D4F (workspace_backup_id), INDEX IDX_F8FFAB387775CCA (machine_backup_id), INDEX IDX_F8FFAB315285974 (event_backup_id), INDEX IDX_F8FFAB32B9484E3 (person_backup_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE units_consummable (id INT AUTO_INCREMENT NOT NULL, name_unit VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE units_consummable_backup (id INT AUTO_INCREMENT NOT NULL, name_unit VARCHAR(50) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE workspaces (id INT AUTO_INCREMENT NOT NULL, name_workspace VARCHAR(100) NOT NULL, description VARCHAR(255) DEFAULT NULL, member_access TINYINT(1) DEFAULT NULL, is_booked TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE workspaces_backup (id INT AUTO_INCREMENT NOT NULL, name_workspace VARCHAR(100) NOT NULL, description VARCHAR(250) DEFAULT NULL, member_access TINYINT(1) DEFAULT NULL, is_booked TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE consummables ADD CONSTRAINT FK_FEB893BAADC9B221 FOREIGN KEY (cat_consummables_id) REFERENCES cat_consummables (id)');
        $this->addSql('ALTER TABLE consummables ADD CONSTRAINT FK_FEB893BABC2655D7 FOREIGN KEY (unit_consummables_id) REFERENCES units_consummable (id)');
        $this->addSql('ALTER TABLE consummables_backup ADD CONSTRAINT FK_9F3A606D67BBBA2E FOREIGN KEY (unit_consummables_backup_id) REFERENCES units_consummable_backup (id)');
        $this->addSql('ALTER TABLE consummables_backup ADD CONSTRAINT FK_9F3A606D2975FE03 FOREIGN KEY (cat_consummables_backup_id) REFERENCES cat_consummables_backup (id)');
        $this->addSql('ALTER TABLE machines ADD CONSTRAINT FK_F1CE8DED82D40A1F FOREIGN KEY (workspace_id) REFERENCES workspaces (id)');
        $this->addSql('ALTER TABLE machines_backup ADD CONSTRAINT FK_6E65D18647915D4F FOREIGN KEY (workspace_backup_id) REFERENCES workspaces_backup (id)');
        $this->addSql('ALTER TABLE participants ADD CONSTRAINT FK_7169709271F7E88B FOREIGN KEY (event_id) REFERENCES events (id)');
        $this->addSql('ALTER TABLE participants ADD CONSTRAINT FK_71697092217BBB47 FOREIGN KEY (person_id) REFERENCES persons (id)');
        $this->addSql('ALTER TABLE participants_backup ADD CONSTRAINT FK_F2178E3E15285974 FOREIGN KEY (event_backup_id) REFERENCES events_backup (id)');
        $this->addSql('ALTER TABLE participants_backup ADD CONSTRAINT FK_F2178E3E2B9484E3 FOREIGN KEY (person_backup_id) REFERENCES persons_backup (id)');
        $this->addSql('ALTER TABLE persons ADD CONSTRAINT FK_A25CC7D3708A0E0 FOREIGN KEY (gender_id) REFERENCES genders (id)');
        $this->addSql('ALTER TABLE persons ADD CONSTRAINT FK_A25CC7D31CED915 FOREIGN KEY (postalcode_id) REFERENCES postal_code (id)');
        $this->addSql('ALTER TABLE persons ADD CONSTRAINT FK_A25CC7D38BAC62AF FOREIGN KEY (city_id) REFERENCES cities (id)');
        $this->addSql('ALTER TABLE users_consummables ADD CONSTRAINT FK_ADFCB5B667B3B43D FOREIGN KEY (users_id) REFERENCES persons (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_consummables ADD CONSTRAINT FK_ADFCB5B6315E5C6F FOREIGN KEY (consummables_id) REFERENCES consummables (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE persons_backup ADD CONSTRAINT FK_4A84F2BF7D78D8B8 FOREIGN KEY (postal_code_backup_id) REFERENCES postal_code_backup (id)');
        $this->addSql('ALTER TABLE persons_backup ADD CONSTRAINT FK_4A84F2BF37E97B5C FOREIGN KEY (city_backup_id) REFERENCES cities_backup (id)');
        $this->addSql('ALTER TABLE persons_backup ADD CONSTRAINT FK_4A84F2BFFBA2F1A6 FOREIGN KEY (gender_backup_id) REFERENCES genders_backup (id)');
        $this->addSql('ALTER TABLE persons_backup_consummables_backup ADD CONSTRAINT FK_52D07603641CB851 FOREIGN KEY (persons_backup_id) REFERENCES persons_backup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE persons_backup_consummables_backup ADD CONSTRAINT FK_52D0760385BF2FC9 FOREIGN KEY (consummables_backup_id) REFERENCES consummables_backup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE publications ADD CONSTRAINT FK_32783AF4A76ED395 FOREIGN KEY (user_id) REFERENCES persons (id)');
        $this->addSql('ALTER TABLE publications_backup ADD CONSTRAINT FK_6093970A641CB851 FOREIGN KEY (persons_backup_id) REFERENCES persons_backup (id)');
        $this->addSql('ALTER TABLE reservations ADD CONSTRAINT FK_4DA23971F7E88B FOREIGN KEY (event_id) REFERENCES events (id)');
        $this->addSql('ALTER TABLE reservations ADD CONSTRAINT FK_4DA23982D40A1F FOREIGN KEY (workspace_id) REFERENCES workspaces (id)');
        $this->addSql('ALTER TABLE reservations ADD CONSTRAINT FK_4DA239F6B75B26 FOREIGN KEY (machine_id) REFERENCES machines (id)');
        $this->addSql('ALTER TABLE reservations ADD CONSTRAINT FK_4DA239A76ED395 FOREIGN KEY (user_id) REFERENCES persons (id)');
        $this->addSql('ALTER TABLE reservations_backup ADD CONSTRAINT FK_F8FFAB347915D4F FOREIGN KEY (workspace_backup_id) REFERENCES workspaces_backup (id)');
        $this->addSql('ALTER TABLE reservations_backup ADD CONSTRAINT FK_F8FFAB387775CCA FOREIGN KEY (machine_backup_id) REFERENCES machines_backup (id)');
        $this->addSql('ALTER TABLE reservations_backup ADD CONSTRAINT FK_F8FFAB315285974 FOREIGN KEY (event_backup_id) REFERENCES events_backup (id)');
        $this->addSql('ALTER TABLE reservations_backup ADD CONSTRAINT FK_F8FFAB32B9484E3 FOREIGN KEY (person_backup_id) REFERENCES persons_backup (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE consummables DROP FOREIGN KEY FK_FEB893BAADC9B221');
        $this->addSql('ALTER TABLE consummables DROP FOREIGN KEY FK_FEB893BABC2655D7');
        $this->addSql('ALTER TABLE consummables_backup DROP FOREIGN KEY FK_9F3A606D67BBBA2E');
        $this->addSql('ALTER TABLE consummables_backup DROP FOREIGN KEY FK_9F3A606D2975FE03');
        $this->addSql('ALTER TABLE machines DROP FOREIGN KEY FK_F1CE8DED82D40A1F');
        $this->addSql('ALTER TABLE machines_backup DROP FOREIGN KEY FK_6E65D18647915D4F');
        $this->addSql('ALTER TABLE participants DROP FOREIGN KEY FK_7169709271F7E88B');
        $this->addSql('ALTER TABLE participants DROP FOREIGN KEY FK_71697092217BBB47');
        $this->addSql('ALTER TABLE participants_backup DROP FOREIGN KEY FK_F2178E3E15285974');
        $this->addSql('ALTER TABLE participants_backup DROP FOREIGN KEY FK_F2178E3E2B9484E3');
        $this->addSql('ALTER TABLE persons DROP FOREIGN KEY FK_A25CC7D3708A0E0');
        $this->addSql('ALTER TABLE persons DROP FOREIGN KEY FK_A25CC7D31CED915');
        $this->addSql('ALTER TABLE persons DROP FOREIGN KEY FK_A25CC7D38BAC62AF');
        $this->addSql('ALTER TABLE users_consummables DROP FOREIGN KEY FK_ADFCB5B667B3B43D');
        $this->addSql('ALTER TABLE users_consummables DROP FOREIGN KEY FK_ADFCB5B6315E5C6F');
        $this->addSql('ALTER TABLE persons_backup DROP FOREIGN KEY FK_4A84F2BF7D78D8B8');
        $this->addSql('ALTER TABLE persons_backup DROP FOREIGN KEY FK_4A84F2BF37E97B5C');
        $this->addSql('ALTER TABLE persons_backup DROP FOREIGN KEY FK_4A84F2BFFBA2F1A6');
        $this->addSql('ALTER TABLE persons_backup_consummables_backup DROP FOREIGN KEY FK_52D07603641CB851');
        $this->addSql('ALTER TABLE persons_backup_consummables_backup DROP FOREIGN KEY FK_52D0760385BF2FC9');
        $this->addSql('ALTER TABLE publications DROP FOREIGN KEY FK_32783AF4A76ED395');
        $this->addSql('ALTER TABLE publications_backup DROP FOREIGN KEY FK_6093970A641CB851');
        $this->addSql('ALTER TABLE reservations DROP FOREIGN KEY FK_4DA23971F7E88B');
        $this->addSql('ALTER TABLE reservations DROP FOREIGN KEY FK_4DA23982D40A1F');
        $this->addSql('ALTER TABLE reservations DROP FOREIGN KEY FK_4DA239F6B75B26');
        $this->addSql('ALTER TABLE reservations DROP FOREIGN KEY FK_4DA239A76ED395');
        $this->addSql('ALTER TABLE reservations_backup DROP FOREIGN KEY FK_F8FFAB347915D4F');
        $this->addSql('ALTER TABLE reservations_backup DROP FOREIGN KEY FK_F8FFAB387775CCA');
        $this->addSql('ALTER TABLE reservations_backup DROP FOREIGN KEY FK_F8FFAB315285974');
        $this->addSql('ALTER TABLE reservations_backup DROP FOREIGN KEY FK_F8FFAB32B9484E3');
        $this->addSql('DROP TABLE cat_consummables');
        $this->addSql('DROP TABLE cat_consummables_backup');
        $this->addSql('DROP TABLE cities');
        $this->addSql('DROP TABLE cities_backup');
        $this->addSql('DROP TABLE consummables');
        $this->addSql('DROP TABLE consummables_backup');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE events');
        $this->addSql('DROP TABLE events_backup');
        $this->addSql('DROP TABLE genders');
        $this->addSql('DROP TABLE genders_backup');
        $this->addSql('DROP TABLE machines');
        $this->addSql('DROP TABLE machines_backup');
        $this->addSql('DROP TABLE participants');
        $this->addSql('DROP TABLE participants_backup');
        $this->addSql('DROP TABLE persons');
        $this->addSql('DROP TABLE users_consummables');
        $this->addSql('DROP TABLE persons_backup');
        $this->addSql('DROP TABLE persons_backup_consummables_backup');
        $this->addSql('DROP TABLE postal_code');
        $this->addSql('DROP TABLE postal_code_backup');
        $this->addSql('DROP TABLE publications');
        $this->addSql('DROP TABLE publications_backup');
        $this->addSql('DROP TABLE reservations');
        $this->addSql('DROP TABLE reservations_backup');
        $this->addSql('DROP TABLE units_consummable');
        $this->addSql('DROP TABLE units_consummable_backup');
        $this->addSql('DROP TABLE workspaces');
        $this->addSql('DROP TABLE workspaces_backup');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
